#use wml::debian::template title="Debians utvecklarhörna" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="ac395767b81c6001b3d9b14f921c0fa7b6386605"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Även om all information på
denna sida och alla länkar till andra sidor är publikt tillgängliga, är
denna sida primärt riktad till Debianutvecklare.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Allmän information</a></li>
<li><a href="#packaging">Paketering</a></li>
<li><a href="#workinprogress">Pågående arbete</a></li>
<li><a href="#projects">Projekt</a></li>
<li><a href="#miscellaneous">Blandat</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Allmän information</a></h2>
      <p>En lista på nuvarande utvecklare och paketansvariga, hur man går med i
         projektet och länkar till utvecklardatabasen, stadgarna,
         omröstningsprocessen, utgåvor och arkitekturer.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Debians organisation</a></dt>
        <dd>Mer än tusen frivilliga är en del av Debianprojektet. Denna sida
            förklarar Debians organisationsstruktur, listar grupper och deras
            medlemmar så väl som kontaktadresser.</dd>
        <dt><a href="$(HOME)/intro/people">Folket bakom Debian</a></dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">Debianutvecklare (Debian Developer - DD)</a> (fullständiga medlemmar av Debianprojektet) och
        <a href="https://wiki.debian.org/DebianMaintainer">Debian Maintainers (DM)</a>, bidrar till projektet. Vänligen ta en titt på
        <a href="https://nm.debian.org/public/people/dd_all">listan på Debianutvecklare</a> och 
        <a href="https://nm.debian.org/public/people/dm_all">listan på Debian Maintainers</a> för att
        hitta mer information om folket som är involverat, inklusive paketen som dom är ansvariga för. Vi har också en
        <a href="developers.loc">världskarta med Debianutvecklare utmärkta</a> och ett
        <a href="https://gallery.debconf.org/">galleri</a> med bilder från olika Debianevenemang.</dd>
        <dt><a href="join/">Hur man går med i Debian</a></dt>
        <dd>Skulle du vilja bidra till och gå med i projektet? Vi söker alltid
        efter nya utvecklare eller fri mjukvaruentusiaster med tekniska
        kunskaper. För ytterligare information, besök denna sida.</dd>
        <dt><a href="https://db.debian.org/">Utvecklardatabasen</a></dt>
        <dd>En del information i denna databas är tillgänglig för alla, och
        en del är endast tillgänglig för utvecklare som är inloggade. Databasen
        innehåller information som 
        <a href="https://db.debian.org/machines.cgi">projektmaskiner</a> och 
        <a href="extract_key">utvecklares GnuPG-nycklar</a>. Utvecklare med ett
        konto kan <a href="https://db.debian.org/password.html">ändra sina
        lösenord</a> och lära sig hur man sätter upp
        <a href="https://db.debian.org/forward.html">vidarebefordring av e-post</a>
        för deras Debiankonton. Om du planerar att använda någon av
        Debianmaskinerna, vänligen säkerställ att du har läst 
        <a href="dmup">Debians policy för maskinanvändning</a>.</dd>
        <dt><a href="constitution">Stadgarna</a></dt>
        <dd>Detta dokument beskriver organisationsstrukturen för formellt
        beslutsfattande i projektet.
        </dd>
        <dt><a href="$(HOME)/vote/">Omröstningsinformation</a></dt>
        <dd>Hur vi väljer våra ledare och logotyper och hur vi röstar i allmänhet.</dd>
        <dt><a href="$(HOME)/releases/">Versionsfakta</a></dt>
        <dd>Denna sida lista aktuella utgåvor (<a
        href="$(HOME)/releases/stable/">stabila utgåvan</a>, 
        <a href="$(HOME)/releases/testing/">uttestningsutgåvan</a>, och 
        <a href="$(HOME)/releases/unstable/">instabila utgåvan</a>) och innehåller ett
        index över gamla utgåvor och deras kodnamn.</dd>
        <dt><a href="$(HOME)/ports/">Olika arkitekturer</a></dt>
        <dd>Debian kör på många olika arkitekturer. Denna sida samlar
        information om olika Debiananpassningar, några baserade på Linuxkärnan,
        andra baserade på FreeBSD, NetBSD eller Hurd-kärnorna.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Paketering</a></h2>
      <p>Länkar till vår policyhandbok och andra dokument som relaterar till
      Debianpolicyn, procedurer och andra resurser för Debianutvecklare,
      samt Nyunderhållarguiden.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debians policyhandbok</a></dt>
        <dd>Detta dokument beskriver policykraven för Debiandistributionen.
        Detta inkluderar strukturen och innehållet i Debianarkivet, flera
        designfrågor gällande operativsystemet så väl som tekniska
        krav som varje paket måste uppnå för att inkluderas i distributionen.

        <p>I korthet, något du <strong>måste</strong> läsa.</p>
        </dd>
      </dl>

      <p>Det finns flera andra dokument relaterat till policyn som du kan vara
      intresserad av:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />FHS definierar mappstrukturen och kataloginnehållet (placering av
        filer); det är ett krav att följa version 3.0 (se <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">kapitel
        9</a> i Debians policymanual).</li>
        <li>Lista över <a href="$(DOC)/packaging-manuals/build-essential">paket som anses vara build-essential</a>
        <br />Du förväntas ha dessa paket om du vill kompilera mjukvara, bygga
        ett paket eller en uppsättning paket. Du behöver inte inkludera dessa
        i <code>Build-Depends</code>-raden när du <a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">deklarerar
        förhållanden</a> mellan paket.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Menysystem</a>
        <br />Debians struktur på menyposter; vänligen se dokumentationen för
        <a href="$(DOC)/packaging-manuals/menu.html/">menysystemet</a>
        också.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacspolicyn</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Javapolicyn</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perlpolicyn</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Pythonpolicyn</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconfspecifikationen</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Database Application-policyn</a> (draft)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk Policy</a> (draft)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debians policy för Ada</a></li>
      </ul>

      <p>Vänligen ta också en titt på <a
      href="https://bugs.debian.org/debian-policy">föreslagna uppdateringar till
      Debianpolicyn</a>, too.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Utvecklarreferensen</a></dt>

        <dd>
        Överblick över dom rekommenderade procedurerna och dom tillgängliga
        resurserna för Debianutvecklare -- ytterligare en <strong>rekommenderad läsning</strong>
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">New Maintainers' Guide</a></dt>

        <dd>
        Hur du bygger ett Debianpaket (på vanligt språk), inklusive en mängd
        exempel. Om du planerar att bli en Debianutvecklare eller
        paketansvarig så är detta en bra startpunkt.
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">Pågående arbete: Länkar för aktiva Debianutvecklare och paketansvariga</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian &lsquo;Testing&rsquo;</a></dt>
  <dd>
    Genereras automatiskt från &lsquo;unstable&rsquo;-distributionen:
    Det år hit du måste få dina paket för att dom ska övervägas för nästa
    Debianutgåva.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Utgåvekritiska felrapporter</a></dt>
  <dd>
    En lista på fel som kan orsaka att ett paket tas bort från
    &lsquo;uttestnings&rsquo;-distributionen eller kan orsaka en fördröjning
    för nästa utgåva. Felrapporter som har en allvarlighetsgrad högre än
    eller lika med &lsquo;serious&rsquo; kvalificerar för denna lista,
    så se till att rätta dessa fel som är rapporterade mot dina paket så
    snabbt du kan.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Debians felspårningssystem (Debian Bug Tracking System - BTS)</a></dt>
    <dd>
    För rapportering av, diskussioner om och rättning av fel. BTS är användbart för
    både användare och utvecklare.
    </dd>
        
  <dt>Information om Debianpaket</dt>
    <dd>
      Sidorna för <a 
      href="https://qa.debian.org/developer.php">Paketinformation</a> och
      <a href="https://tracker.debian.org/">paketspårning</a> tillhandahåller
      samlingar av värdefull information till paketansvariga. Utvecklare som
      vill hålla koll på andra paket kan prenumerera (via e-post) på
      en tjänst som skickar ut kopior på BTS-e-postmeddelanden och
      notifieringar för uppladdningar och installationer. Vänligen se <a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">manualen
      för paketspåraren</a> för ytterligare information.
    </dd>

    <dt><a href="wnpp/">Paket som behöver hjälp</a></dt>
      <dd>
      Paket som behöver arbetas på och eventuella framtida paket (WNPP - 
      Work-Needing and Prospective Packages) är en lista på Debianpaket som
      behöver en ny paketansvarig och paket som inte har inkluderats i Debian
      ännu.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Inkommande-systemet</a></dt>
      <dd>
      Interna arkivservrar: detta är var nya paket laddas upp. Accepterade paket
      finns nästan direkt tillgängliga via en webb läsare och kommer att
      propagera till <a href="$(HOME)/mirror/">speglingarna</a> fyra
      gånger om dagen.
      <br />
      <strong>OBS:</strong> På grund av arten av &lsquo;incoming&rsquo;,
      så rekommenderar vi inte att spegla det.
      </dd>

    <dt><a href="https://lintian.debian.org/">Lintianrapporter</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      är ett program som kontrollerar om ett paket följer policy. Utvecklare
      bör använda det före varje uppladdning.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian &lsquo;Experimental&rsquo;</a></dt>
      <dd>
      &lsquo;Experimental&rsquo;-distributionen används som en temporär
      plats för experimentell mjukvara. Vänligen installera bara
      <a href="https://packages.debian.org/experimental/">experimentella
      paket</a> om du redan vet hur du hanterar
      &lsquo;unstable&rsquo;.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Debian Wiki</a></dt>
      <dd>
      Debianwikin med råd för utvecklare och andra bidragslämnare.
      </dd>
</dl>

<h2><a id="projects">Projekt: Interna grupper och projekt</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Debians webbsidor</a></li>
<li><a href="https://ftp-master.debian.org/">Debianarkivet</a></li>
<li><a href="$(DOC)/ddp">Debians dokumentationsprojekt (Debian Documentation Project - DDP)</a></li>
<li><a href="https://qa.debian.org/">Debians grupp för kvalitetssäkring (Quality Assurance - QA)</a></li>
<li><a href="$(HOME)/CD/">CD/DVD-avbildningar</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Nyckelsignering</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Koordination av nyckelsignering</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Debians IPv6-projekt</a></li>
<li><a href="buildd/">Automatbyggarnätverket</a> och deras <a href="https://buildd.debian.org/">byggloggar</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Debians projekt för översättningar av beskrivningar (Description Translation Project - DDTP)</a></li>
<li><a href="debian-installer/">Debianinstalleraren</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debiankvinnor</a></li>
<li><a href="$(HOME)/blends/">Debian ren blandning (Debian Pure Blends)</a></li>

</ul>


<h2><a id="miscellaneous">Blandat</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Inspelningar</a> från vara konferensföreläsningar</li>
<li><a href="passwordlessssh">Sätta upp SSH</a> så att det inte efterfrågar ett lösenord</li>
<li>Hur man <a href="$(HOME)/MailingLists/HOWTO_start_list">efterfrågar en ny sändlista</a></li>
<li>Information om hur man <a href="$(HOME)/mirror/">speglar Debian</a></li>
<li><a href="https://qa.debian.org/data/bts/graphs/all.png">Graf över alla felrapporter</a></li>
<li><a href="https://ftp-master.debian.org/new.html">Nya paket</a> som väntar på att bli inkluderade i Debian (NEW Queue)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Nya Debianpaket</a> de senaste 7 dagarna</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Paket som tas bort från Debian</a></li>
</ul>
