# galaxico <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-15 16:17+0200\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: English <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Δεν υπάρχουν αιτήματα για υιοθεσία"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Δεν υπάρχουν ορφανά πακέτα"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Δεν υπάρχουν πακέτα που αναμένουν υιοθεσία"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Δεν υπάρχουν πακέτα που περιμένουν να πακεταριστούν"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Δεν έχουν ζητηθεί πακέτα"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Δεν έχει ζητηθεί βοήθεια"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "σε υιοθεσία από σήμερα."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "σε υιοθεσία από χτες."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "%s ημέρες σε υιοθεσία."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "%s μέρες σε υιοθεσία, πιο πρόσφατη δραστηριότητα σήμερα."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "%s μέρες σε υιοθεσία, πιο πρόσφατη δραστηριότητα χτες."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "%s μέρες σε υιοθεσία, πιο πρόσφατη δραστηριότητα πριν από %s ημέρες."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "σε προετοιμασία από σήμερα."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "σε προετοιμασία από χτες."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "%s μέρες σε προετοιμασία."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "%s μέρες σε προετοιμασία, τελευταία δραστηριότητα σήμερα."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "%s μέρες σε προετοιμασία, τελευταία δραστηριότητα χτες."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "%s μέρες σε προετοιμασία, τελευταία δραστηριότητα πριν από %s μέρες."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "ζητήθηκε υιοθεσία από χτες."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "ζητήθηκε υιοθεσία από χτες."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "ζητήθηκε υιοθεσία πριν από %s μέρες."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "ορφανό από σήμερα."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "ορφανό από χτες."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "ορφανό πριν από %s μέρες."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "ζητήθηκε σήμερα."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "ζητήθηκε χτες."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "ζητήθηκε πριν από %s μέρες."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "πληροφορίες πακέτου"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "βαθμός:"
