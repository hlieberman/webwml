# galaxico <galas@tee.gr>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2020-09-26 15:20+0300\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.3\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "ηλεκτρονική διεύθυνση αντιπροσωπείας"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "ηλεκτρονική διεύθυνση διορισμένης θέσης"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>αντιπρόσωπος"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>αντιπρόσωπος (η)"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>αυτός/αυτόν"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>αυτή/αυτήν"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>αντιπρόσωπος"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>αυτοί-ες/αυτούς-ες"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "Τωρινός/Τωρινή"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "Μέλος"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "Διαχειριστής"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Διαχειριστής Σταθερής Έκδοσης"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "wizard"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "Προεδρεύων/ουσα"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "Βοηθός"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "Γραμματέας"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "αντιπρόσωπος"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "ρόλος"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"Στην παρακάτω λίστα, το <q>current</q> χρησιμοποιείται για θέσεις που είναι\n"
"μεταβατικές (εκλεγμένες ή διορισμένες με μια συγκεκριμένη ημερομηνία λήξης)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Aξιωματούχοι"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Διανομή"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Επικοινωνία και Απεύθυνση"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Ομάδα Προστασίας Δεδομένων"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Ομάδα Δημοσιότητας"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Συμμετοχή σε άλλους οργανισμούς"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:305
msgid "Support and Infrastructure"
msgstr "Υποστήριξη και Υποδομή"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Επικεφαλής"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Τεχνική Επιτροπή"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Γραμματέας"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Έργα Ανάπτυξης"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Αρχειοθήκες FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Διαχειριστές FTP"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Βοηθοί FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP Wizards"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Ομάδα Backports"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Διαχείριση Έκδοσης"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Ομάδα Έκδοσης"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Εξασφάλιση Ποιότητας"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Ομάδα Συστήματος Εγκατάστασης"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr "Ομάδα του Debian Live"

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Σημειώσεις Έκδοσης"

#: ../../english/intro/organization.data:153
msgid "CD/DVD/USB Images"
msgstr "Εικόνες CD/DVD/USB"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Παραγωγή"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Δοκιμαστική"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Ομάδα Νέφους"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Υποδομή Αυτόματης Μεταγλώττισης"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Επίδοξη Ομάδα Μεταγλώττισης (Wanna-build team)"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Διαχείριση Buildd"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Τεκμηρίωση"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Λίστα Αναμενόμενων πακέτων και Πακέτων που χρειάζονται Εργασία"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Υπεύθυνος Τύπου"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Ιστοσελίδες"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Πλανήτης Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Απεύθυνση"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Σχέδιο Debian Women"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Κοινότητα´"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Για να στείλετε ένα ιδιωτικό μήνυμα στην Ομάδα Επικοινωνίας, χρησιμοποιήστε "
"το GPG κλειδί <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Γεγονότα"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "Επιτροπή DebConf"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Πρόγραμμα Συνεργατών"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Συντονισμός Δωρεών Υλικού"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "Ίδρυμα GNOME"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:292
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:295
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:298
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:301
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:308
msgid "Bug Tracking System"
msgstr "Σύστημα Παρακολούθησης Σφαλμάτων (BTS)"

#: ../../english/intro/organization.data:313
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Διαχείριση Λιστών αλληλογραφίας και αρχειοθήκες αλληλογραφίας"

#: ../../english/intro/organization.data:321
msgid "New Members Front Desk"
msgstr "Υποδοχή Νέων Μελών"

#: ../../english/intro/organization.data:327
msgid "Debian Account Managers"
msgstr "Διαχειριστές Λογαριασμών Debian"

#: ../../english/intro/organization.data:331
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Για να στείλετε ένα ιδιωτικό μήνυμα σε όλους τους DAM, χρησιμοποιήστε το GPG "
"κλειδί 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:332
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Διαχειριστές Κλειδοθήκης (PGP και GPG)"

#: ../../english/intro/organization.data:336
msgid "Security Team"
msgstr "Ομάδα Ασφάλειας"

#: ../../english/intro/organization.data:347
msgid "Policy"
msgstr "Πολιτική"

#: ../../english/intro/organization.data:350
msgid "System Administration"
msgstr "Διαχείριση Συστήματος"

#: ../../english/intro/organization.data:351
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Αυτή είναι η διεύθυνση που θα χρησιμοποιήσετε, όταν αντιμετωπίζετε "
"προβλήματα με τα μηχανήματα του Debian (συμπεριλαμβανομένων και των κωδικών "
"πρόσβασης ή της ανάγκης για εγκατάσταση πακέτων)."

#: ../../english/intro/organization.data:361
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Αν διαπιστώσετε προβλήματα υλικού με τα μηχανήματα Debian, δείτε τη σελίδα "
"<a href=https://db.debian.org/machines.cgi>Debian Machines</a>, περιλαμβάνει "
"πληροφορίες για τους διαχειριστές (ανά μηχάνημα)."

#: ../../english/intro/organization.data:362
msgid "LDAP Developer Directory Administrator"
msgstr "Διαχειριστής του LDAP Καταλόγου Προγραμματιστριών/στών"

#: ../../english/intro/organization.data:363
msgid "Mirrors"
msgstr "Καθρέφτες αρχειοθηκών"

#: ../../english/intro/organization.data:369
msgid "DNS Maintainer"
msgstr "Συντηρητής DNS"

#: ../../english/intro/organization.data:370
msgid "Package Tracking System"
msgstr "Σύστημα Παρακολούθησης Πακέτων (PTS)"

#: ../../english/intro/organization.data:372
msgid "Treasurer"
msgstr "Υπεύθυνος Οικονομικών"

#: ../../english/intro/organization.data:379
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Αιτήματα χρήσης του <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Εμπορικού Σήματος</a>"

#: ../../english/intro/organization.data:383
msgid "Salsa administrators"
msgstr "Διαχειριστές Salsa"

#~ msgid "APT Team"
#~ msgstr "Ομάδα APT"

#~ msgid "Accountant"
#~ msgstr "Λογιστήριο"

#~ msgid "Alioth administrators"
#~ msgstr "Διαχειριστές Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Αντι-παρενόχληση"

#~ msgid "CD Vendors Page"
#~ msgstr "Σελίδα Πωλητών CD"

#~ msgid "Consultants Page"
#~ msgstr "Σελίδα Συμβούλων"

#, fuzzy
#~ msgid "Custom Debian Distributions"
#~ msgstr "Debian Διανομή Πολυμέσων"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux για Μεγάλες Επιχειρήσεις"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debian Διανομή Πολυμέσων"

#~ msgid "Debian Pure Blends"
#~ msgstr "Καθαρλα \"Μείγματα\" του Debian"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian για την Αστρονομία"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian για παιδιά από 1 έως 99"

#~ msgid "Debian for education"
#~ msgstr "Debian για την εκπαίδευση"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian για ιατρική πρακτική και έρευνα"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian για μή-κερδοσκοπικούς οργανισμούς"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian για άτομα με ειδικές ανάγκες"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian για τις επιστήμες και τη σχετική έρευνα"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian σε νομικά γραφεία"

#~ msgid "Delegates"
#~ msgstr "Απεσταλμένοι"

#~ msgid "Embedded systems"
#~ msgstr "Ενσωματωμένα συστήματα"

#~ msgid "Firewalls"
#~ msgstr "Τείχη Προστασίας"

#~ msgid "Handhelds"
#~ msgstr "Υπολ. παλάμης"

#~ msgid "Individual Packages"
#~ msgstr "Μεμονωμένα Πακέτα"

#~ msgid "Installation"
#~ msgstr "Εγκατάσταση"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Σύστημα Εγκατάστασης για τη ``stable''"

#~ msgid "Key Signing Coordination"
#~ msgstr "Συντονισμός Υπογραφών Κλειδιών"

#~ msgid "Laptops"
#~ msgstr "Φορητοί"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Ομάδα Συστήματος Εγκατάστασης"

#~ msgid "Mailing List Archives"
#~ msgstr "Αρχειοθήκες Συνδρομητικών Καταλόγων"

#~ msgid "Mailing list"
#~ msgstr "Συνδρομητικός κατάλογος"

#~ msgid "Ports"
#~ msgstr "Υλοποιήσεις"

#~ msgid "Publicity"
#~ msgstr "Δημοσιότητα"

#~ msgid "Release Assistants"
#~ msgstr "Βοηθοί Έκδοσης"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Επικεφαλής έκδοσής για τη ``stable''"

#~ msgid "Security Audit Project"
#~ msgstr "Εργο επιθεώρησης ασφάλειας"

#, fuzzy
#~ msgid "Security Testing Team"
#~ msgstr "Ομάδα Ασφάλειας"

#~ msgid "Special Configurations"
#~ msgstr "Ιδιαίτερες Διατάξεις"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Ομάδα Ασφάλειας"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Το Διεθνές Λειτουργικό Σύστημα στην επιφάνεια εργασίας σας"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Αυτό δεν είναι ακόμα ένα επίσημο εσωτερικό έργο αλλά, έχει ανακοινωθεί η "
#~ "πρόθεση ενσωμάτωσης του."

#~ msgid "User support"
#~ msgstr "Υποστήριξη χρηστών"

#~ msgid "Vendors"
#~ msgstr "Πωλητές"
