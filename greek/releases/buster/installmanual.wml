#use wml::debian::template title="Debian buster -- Οδηγός Εγκατάστασης" 
#BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#use wml::debian::translation-check translation="91954674647a3fceb1158db18ea7d17bc1093ce9" maintainer="galaxico"

<if-stable-release release="stretch">
<p>Αυτή είναι μια <strong>beta έκδοση</strong> του Οδηγού Εγκατάστασης για 
το Debian 10, που έχει την κωδική ονομασία buster, που δεν έχει ακόμα 
κυκλοφορήσει. Οι πληροφορίες που περιλαμβάνονται εδώ ίσως είναι παρωχημένες 
ή/και λανθασμένες εξαιτίας αλλαγών στον εγκαταστάτη. Ίσως ενδιαφέρεστε να 
δείτε τον
<a href="../stretch/installmanual">Οδηγό Εγκατάστασης για το Debian
9, με την κωδική ονομασία stretch</a>, που είναι η έκδοση του Debian που έχει 
κυκλοφορήσει πιο πρόσφατα· ή την <a 
href="https://d-i.debian.org/manual/">Έκδοση υπό ανάπτυξη του Οδηγού 
Εγκατάστασης</a>, που είναι η πιο επικαιροποιημένη έκδοση του παρόντος 
κειμένου.</p>
</if-stable-release>

<p>Οδηγίες εγκατάστασης μαζί με αρχεία προς μεταφόρτωση, είναι 
διαθέσιμα για κάθε μια από τις υποστηριζόμενες αρχιτεκτονικές:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>Αν έχετε κάνει σωστά τις ρυθμίσεις τοπικοποίησης του περιηγητή 
ιστοσελίδων, μπορείτε να χρησιμοποιήσετε τους παραπάνω συνδέσμους για να 
πάρετε τη σωστή έκδοση σε HTML αυτόματα &mdash; δείτε τη σελίδα <a 
href="$(HOME)/intro/cn">διαπραγμάτευση περιεχομένου</a>.
Διαφορετικά, επιλέξτε την ακριβή αρχιτεκτονική, γλώσσα και 
μορφοποίηση του κειμένου που θέλετε από τον πίνακα που ακολουθεί.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Αρχιτεκτονική</strong></th>
  <th align="left"><strong>Μορφοποίηση</strong></th>
  <th align="left"><strong>Γλώσσες</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   $_[2] eq html ? "$_[0].$_[1].$_[2]" : "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
