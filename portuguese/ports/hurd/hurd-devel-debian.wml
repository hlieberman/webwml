#use wml::debian::template title="Debian GNU/Hurd --- Desenvolvimento" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="dd86de49807cb957ea73388b4bf2565163644a0c"

<h1>
Debian GNU/Hurd</h1>
<h2>
Desenvolvimento da distribuição</h2>

<h3>
Portando pacotes do Debian</h3>
<p>
Se você quer ajudar o porte do Debian GNU/Hurd, você deve se familiarizar
com o sistema de empacotamento do Debian. Uma vez que você tenha feito isso
lendo a documentação disponível e visitando o <a
href="$(HOME)/devel/">canto dos(as) desenvolvedores(as)</a>, você deve saber
como extrair pacotes-fonte do Debian e construir um pacote Debian. Aqui está um
curso rápido para as pessoas realmente preguiçosas:</p>

<h3>
Obtendo fontes e construindo pacotes</h3>

<p>
A obtenção de código-fonte pode ser feita ao simplesmente executar
<code>apt-get source pacote</code>, que também vai extrair o fonte.
</p>

<p>
A extração do pacote-fonte do Debian requer o arquivo
<code>package_version.dsc</code> e os arquivos listados nele. Você constrói o
diretório de construção do Debian com o comando
<code>dpkg-source -x package_version.dsc</code>
</p>

<p>
A construção de um pacote é feita no agora existente diretório de construção do
Debian <code>package-version</code> com o comando
<code>dpkg-buildpackage -B "-mMyName &lt;MyEmail&gt;"</code>.
Em vez de <code>-B</code>, você pode usar
<code>-b</code> se você também quiser construir partes do pacote
independente de arquitetura. Você pode adicionar
<code>-uc</code> para evitar a assinatura do pacote com sua chave pgp.</p>

<p>
A construção pode requerer pacotes adicionais instalados. A forma mais simples
é executar <code>apt-get build-dep pacote</code>, que vai instalar todos os
pacotes requeridos.
</p>

<p>
Usar o pbuilder pode ser conveniente. Ele pode ser construído com
<code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/ --debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg --debootstrapopts --extra-suites=unreleased --extrapackages debian-ports-archive-keyring</code>
e então pode-se usar <code>pdebuild -- --binary-arch</code>, o que vai lidar com
o download de dependências de construção e outras coisas, e colocar o resultado
em <code>/var/cache/pbuilder/result</code>
</p>

<h3>
Escolha um</h3>
<p>
Quais pacotes precisam ser trabalhados? Bem, cada pacote que ainda não
foi portado, mas precisa ser portado. Isso muda constantemente, então
é preferível se concentrar primeiro em pacotes com muitas dependências
reversas, o que pode ser visto no gráfico de dependência de pacote
<url "https://people.debian.org/~sthibault/graph-radial.pdf"> atualizado todo
dia, ou na lisa de mais procurados
<url "https://people.debian.org/~sthibault/graph-total-top.txt"> (esta é a lista
de mais procurados no longo prazo, a lista de mais procurados no curto prazo é
<url "https://people.debian.org/~sthibault/graph-top.txt">).
Usualmente, é também uma boa ideia escolher nas listas de desatualizados
<url "https://people.debian.org/~sthibault/out_of_date2.txt"> e
<url "https://people.debian.org/~sthibault/out_of_date.txt">, já que esses
estavam funcionando e agora estão quebrados provavelmente devido a alguns
poucos motivos. Você também pode só escolher um dos pacotes ausentes
aleatoriamente, ou verificar os logs de autoconstrução na lista de discussão
debian-hurd-build-logs, ou usar a
lista wanna-build de
<url "https://people.debian.org/~sthibault/failed_packages.txt">.
Alguns problemas de construção são mais fáceis de consertar que outros.
Tipicamente, "undefined reference to foo" (referência não definida para foo),
onde foo é algo como pthread_create, dlopen, cos, ... (o que está obviamente
disponível em hurd-i386), que somente mostra que a etapa de configuração
do pacote esqueceu de incluir -lpthread, -ldl, -lm, etc., também no Hurd.
Note, contudo, que funções ALSA MIDI não estão disponíveis.
</p>
<p>
Além disso, verifique se o trabalho já feito feito em
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse">,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse">
e no BTS (<url "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd">),
e <url "https://wiki.debian.org/Debian_GNU/Hurd">,
e no estado atual dos pacotes em buildd.debian.org, por exemplo,
<url "https://buildd.debian.org/util-linux">.
</p>

<h4>
Pacotes que não serão portados</h4>
<p>
Alguns desses pacotes, ou partes deles, podem ser portados posteriormente, mas
atualmente eles são considerados, no mínimo, não portáveis. Normalmente eles são
marcados como NotForUs no banco de dados buildd.
</p>

<ul>
<li>
<code>base/makedev</code>, porque o Hurd vem com sua própria versão
deste script. O pacote-fonte do Debian somente contém uma versão
Linux específica.</li>
<li>
<code>base/modconf</code> e <code>base/modutils</code>, porque
módulos são um conceito específico ao Linux.</li>
<li>
<code>base/netbase</code>, porque o restante das coisas que estão ali
são muito específicas ao kernel Linux. O Hurd usa
<code>inetutils</code> em vez disso.</li>
<li>
<code>base/pcmcia-cs</code>, porque este pacote é específico ao Linux.</li>
<li>
<code>base/setserial</code>, porque é específico ao kernel
Linux. Entretanto, com o porte dos drivers de conjunto de caracteres Linux para
o GNU Mach, nós poderíamos ser capazes de usá-lo.</li>
</ul>

<h3> <a name="porting_issues">
Problemas gerais de porte</a></h3>
<p>
<a href=https://www.gnu.org/software/hurd/hurd/porting/guidelines.html>Uma lista
de problemas comuns</a> está disponível no site web do(a) autor(a) original
(upstream). Os seguintes problemas comuns são específicos ao Debian.</p>
<p>Antes de tentar corrigir alguma coisa, verifique se no porte do kfreebsd*
talvez tenha alguma correção recente, que só precisa ser estendida para o
hurd-i386.</p>

<ul>
<li>
<code>Dependência quebrada do libc6</code>
<p>
Alguns pacotes usam uma dependência errônea em <code>libc6-dev</code>. Isto
está incorreto porque o <code>libc6</code> é específico de algumas arquiteturas
do GNU/Linux. O pacote correspondente para o GNU é <code>libc0.3-dev</code>,
mas outros SOs terão pacotes diferentes. Você pode localizar o problema no
arquivo <code>debian/control</code> da árvore-fonte. Soluções típicas incluem
detectar o SO usando <code>dpkg-architecture</code> e inserir no próprio código
o soname, ou melhor, use um OU lógico, por exemplo:
<code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.
O <code>libc-dev</code> é um
pacote virtual que funciona para qualquer soname, mas você tem que colocá-lo
somente como a última opção.</p></li>
<li>
<code>undefined reference to snd_*, SND_* undeclared</code>
<p>
Alguns pacotes usam ALSA mesmo em arquiteturas não Linux. O pacote oss-libsalsa
fornece alguma emulação sobre OSS, mas é limitado à 1.0.5, e algumas
funcionalidades não são fornecidas, como todas as operações de sequenciador.
</p>
<p>
Se o pacote permitir, o suporte a alsa deve ser desabilitado nas
arquiteturas <code>!linux-any</code> (por exemplo, por meio de uma opção
<code>configure</code>), e um qualificador <code>[linux-any]</code> deve ser
adicionado ao <code>Build-Depends</code> do alsa, e o oposto adicionado para
<code>Build-Conflicts</code>, como
<code>Build-Conflicts: libasound2-dev [!linux-any]</code>.
</p>
</li>
<li>
<code>dh_install: Cannot find (any matches for) "foo" (tried in ., debian/tmp)</code>
<p>
Isso tipicamente acontece quando o(a) autor(a) original (upstream) não instalou
algo porque o SO não foi reconhecido. Algumas vezes é uma coisa besta (por
exemplo, não sabe que a construção de uma biblioteca compartilhada no GNU/Hurd é
exatamente como no GNU/Linux) e isso precisa de correção. Algumas vezes faz
realmente sentido (por exemplo, a não instalação de arquivos de serviço
systemd). Neste caso, pode-se usar dh-exec: construa a dependência em
<tt>dh-exec</tt>, <tt>chmod +x</tt> o arquivo <tt>.install</tt> e prefixe as
linhas problemáticas com, por exemplo, <tt>[linux-any]</tt> ou
<tt>[!hurd-any]</tt>.
</p>
</li>
</ul>
