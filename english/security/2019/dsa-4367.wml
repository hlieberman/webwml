<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered multiple vulnerabilities in
systemd-journald. Two memory corruption flaws, via attacker-controlled
allocations using the alloca function (<a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a>, 
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16865">CVE-2018-16865</a>)
and an out-of-bounds read flaw leading to an information leak
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-16866">CVE-2018-16866</a>), 
could allow an attacker to cause a denial of service or the execution of
arbitrary code.</p>

<p>Further details in the Qualys Security Advisory at
<a href="https://www.qualys.com/2019/01/09/system-down/system-down.txt">https://www.qualys.com/2019/01/09/system-down/system-down.txt</a></p>

<p>For the stable distribution (stretch), these problems have been fixed in
version 232-25+deb9u7.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>For the detailed security status of systemd please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/systemd">https://security-tracker.debian.org/tracker/systemd</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4367.data"
# $Id: $
