<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Pavel Toporkov discovered a vulnerability in Neutron, the OpenStack
virtual network service, which allowed a reconfiguration of dnsmasq
via crafted dhcp_extra_opts parameters.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2:13.0.7+git.2021.09.27.bace3d1890-0+deb10u1. This update
also fixes <a href="https://security-tracker.debian.org/tracker/CVE-2021-20267">\
CVE-2021-20267</a>.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2:17.2.1-0+deb11u1. This update also fixes
<a href="https://security-tracker.debian.org/tracker/CVE-2021-38598">\
CVE-2021-38598</a>.</p>

<p>We recommend that you upgrade your neutron packages.</p>

<p>For the detailed security status of neutron please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/neutron">\
https://security-tracker.debian.org/tracker/neutron</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4983.data"
# $Id: $
