<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was reported that the BlueZ's HID and HOGP profile implementations
don't specifically require bonding between the device and the host.
Malicious devices can take advantage of this flaw to connect to a target
host and impersonate an existing HID device without security or to cause
an SDP or GATT service discovery to take place which would allow HID
reports to be injected to the input subsystem from a non-bonded source.</p>

<p>For the HID profile an new configuration option (ClassicBondedOnly) is
introduced to make sure that input connections only come from bonded
device connections. The options defaults to <q>false</q> to maximize device
compatibility.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 5.43-2+deb9u2.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 5.50-1.2~deb10u1.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>For the detailed security status of bluez please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4647.data"
# $Id: $
