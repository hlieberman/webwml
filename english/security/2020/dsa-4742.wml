<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Tim Starling discovered two vulnerabilities in firejail, a sandbox
program to restrict the running environment of untrusted applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17367">CVE-2020-17367</a>

    <p>It was reported that firejail does not respect the end-of-options
    separator ("--"), allowing an attacker with control over the command
    line options of the sandboxed application, to write data to a
    specified file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17368">CVE-2020-17368</a>

    <p>It was reported that firejail when redirecting output via --output
    or --output-stderr, concatenates all command line arguments into a
    single string that is passed to a shell. An attacker who has control
    over the command line arguments of the sandboxed application could
    take advantage of this flaw to run arbitrary commands.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 0.9.58.2-2+deb10u1.</p>

<p>We recommend that you upgrade your firejail packages.</p>

<p>For the detailed security status of firejail please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firejail">https://security-tracker.debian.org/tracker/firejail</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4742.data"
# $Id: $
