<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Tomcat servlet and JSP
engine, which could result in HTTP request smuggling, code execution
in the AJP connector (disabled by default in Debian) or a man-in-the-middle
attack against the JMX interface.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 9.0.31-1~deb10u1. The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">\
CVE-2020-1938</a> may require
configuration changes when Tomcat is used with the AJP connector, e.g.
in combination with libapache-mod-jk. For instance the attribute
<q>secretRequired</q> is set to true by default now. For affected setups it's
recommended to review <a href="https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html">\
https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html</a>
before the deploying the update.</p>

<p>We recommend that you upgrade your tomcat9 packages.</p>

<p>For the detailed security status of tomcat9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4680.data"
# $Id: $
