<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in subversion:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17525">CVE-2020-17525</a>:

    <p>Subversion's mod_authz_svn module will crash if the server is using
    in-repository authz rules with the AuthzSVNReposRelativeAccessFile
    option and a client sends a request for a non-existing repository URL.
    This can lead to disruption for users of the service.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
1.9.5-1+deb9u6.</p>

<p>We recommend that you upgrade your subversion packages.</p>

<p>For the detailed security status of subversion please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/subversion">https://security-tracker.debian.org/tracker/subversion</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2646.data"
# $Id: $
