<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A couple of vulnerabilities were found in paramiko, an implementation
of SSHv2 protocol in Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000805">CVE-2018-1000805</a>

     <p>Fix to prevent malicious clients to trick the Paramiko server
     into thinking an unauthenticated client is authenticated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7750">CVE-2018-7750</a>

     <p>Fix check whether authentication is completed before processing
     other requests. A customized SSH client can simply skip the
     authentication step.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.0.0-1+deb9u1.</p>

<p>We recommend that you upgrade your paramiko packages.</p>

<p>For the detailed security status of paramiko please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/paramiko">https://security-tracker.debian.org/tracker/paramiko</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2860.data"
# $Id: $
