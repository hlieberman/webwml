<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12150">CVE-2017-12150</a>

    <p>Stefan Metzmacher discovered multiple code paths where SMB signing
    was not enforced.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12163">CVE-2017-12163</a>

    <p>Yihan Lian and Zhibin Hu discovered that insufficient range checks
    in the processing of SMB1 write requests could result in disclosure
    of server memory.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.6.6-6+deb7u14.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1110.data"
# $Id: $
