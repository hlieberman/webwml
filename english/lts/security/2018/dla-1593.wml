<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Simon Scannell and Robin Peraglie of RIPS Technologies discovered that
passing an absolute path to a file_exists check in phpBB, a full
featured web forum, allows remote code execution through Object
Injection by employing Phar deserialization when an attacker has access
to the Admin Control Panel with founder permissions.</p>

<p>The fix for this issue resulted in the removal of setting the
ImageMagick path. The GD image library can be used as a replacement
and a new event to generate thumbnails was added, so it is possible to
write an extension that uses a different image library to generate
thumbnails.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.0.12-5+deb8u2.</p>

<p>We recommend that you upgrade your phpbb3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1593.data"
# $Id: $
