<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security researchers identified speculative execution side-channel
methods which have the potential to improperly gather sensitive data
from multiple types of computing devices with different vendors’
processors and operating systems.</p>

<p>This update requires an update to the intel-microcode package, which is
non-free. It is related to DLA-1446-1 and adds more mitigations for
additional types of Intel processors.</p>

<p>For more information please also read the official Intel security
advisories at:</p>

<ul>
<li><url "https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00088.html"></li>
<li><url "https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00115.html"></li>
<li><url "https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00161.html"></li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.20180807a.1~deb8u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1506.data"
# $Id: $
