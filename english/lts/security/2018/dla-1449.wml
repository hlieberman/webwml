<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two issues were discovered in OpenSSL, the Secure Sockets Layer toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0732">CVE-2018-0732</a>

    <p>Denial of service by a malicious server that sends a very large
    prime value to the client during TLS handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0737">CVE-2018-0737</a>

    <p>Alejandro Cabrera Aldaya, Billy Brumley, Cesar Pereida Garcia and
    Luis Manuel Alvarez Tapia discovered that the OpenSSL RSA Key
    generation algorithm has been shown to be vulnerable to a cache
    timing side channel attack. An attacker with sufficient access to
    mount cache timing attacks during the RSA key generation process
    could recover the private key.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.1t-1+deb8u9.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1449.data"
# $Id: $
