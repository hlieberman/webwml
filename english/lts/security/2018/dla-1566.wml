<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been discovered in the MySQL database server. The
vulnerabilities are addressed by upgrading MySQL to the new upstream
version 5.5.62, which includes additional changes. Please see the MySQL
5.5 Release Notes and Oracle's Critical Patch Update advisory for
further details:</p>

<ul>
 <li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-61.html"></li>
 <li><a href="https://www.oracle.com/technetwork/security-advisory/cpujul2018-4258247.html">https://www.oracle.com/technetwork/security-advisory/cpujul2018-4258247.html</a></li>
 <li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-62.html">https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-62.html</a></li>
 <li><a href="https://www.oracle.com/technetwork/security-advisory/cpuoct2018-4428296.html">https://www.oracle.com/technetwork/security-advisory/cpuoct2018-4428296.html</a></li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.5.62-0+deb8u1.</p>

<p>We recommend that you upgrade your mysql-5.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1566.data"
# $Id: $
