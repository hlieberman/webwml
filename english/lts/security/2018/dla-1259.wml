<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in the libtiff image processing
library which may result in an application crash and denial of
service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18013">CVE-2017-18013</a>

    <p>NULL pointer dereference via crafted TIFF image</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u18.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1259.data"
# $Id: $
