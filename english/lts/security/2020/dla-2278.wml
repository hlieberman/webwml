<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Squid, a high-performance proxy caching server for
web clients, has been affected by multiple security vulnerabilities.
Due to incorrect input validation and URL request handling it was
possible to bypass access restrictions for restricted HTTP servers
and to cause a denial-of-service.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.23-5+deb9u2.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>For the detailed security status of squid3 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2278.data"
# $Id: $
