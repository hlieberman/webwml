<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that libntlm through 1.5 relies on a fixed buffer size for
tSmbNtlmAuthRequest, tSmbNtlmAuthChallenge, and tSmbNtlmAuthResponse
read and write operations, as demonstrated by a stack-based buffer
over-read in buildSmbNtlmAuthRequest in smbutil.c for a crafted
NTLM request.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4-3+deb8u1.</p>

<p>We recommend that you upgrade your libntlm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2207.data"
# $Id: $
