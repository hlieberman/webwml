<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In RubyGem excon before 0.71.0, there was a race condition around persistent
connections, where a connection which is interrupted (such as by a timeout)
would leave data on the socket. Subsequent requests would then read this data,
returning content from the previous response.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.33.0-2+deb8u1.</p>

<p>We recommend that you upgrade your ruby-excon packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2070.data"
# $Id: $
