<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>There was a possible directory traversal vulnerability in the
Rack::Directory app that is bundled with Rack.</p>

<p>If certain directories exist in a director that is managed by
`Rack::Directory`, an attacker could, using this vulnerability,
read the contents of files on the server that were outside of
the root specified in the Rack::Directory initializer.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.5.2-3+deb8u3.</p>

<p>We recommend that you upgrade your ruby-rack packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2216.data"
# $Id: $
