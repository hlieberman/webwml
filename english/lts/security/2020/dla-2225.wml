<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two memory handling issues were found in gst-plugins-good0.10, a
collection of GStreamer plugins from the <q>good</q> set:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10198">CVE-2016-10198</a>

     <p>An invalid read can be triggered in the aacparse element via a
     maliciously crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5840">CVE-2017-5840</a>

     <p>An out of bounds heap read can be triggered in the qtdemux element
     via a maliciously crafted file.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.10.31-3+nmu4+deb8u3.</p>

<p>We recommend that you upgrade your gst-plugins-good0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2225.data"
# $Id: $
