<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
                 <p><a href="https://security-tracker.debian.org/tracker/CVE-2018-1100">CVE-2018-1100</a> <a href="https://security-tracker.debian.org/tracker/CVE-2018-13259">CVE-2018-13259</a> <a href="https://security-tracker.debian.org/tracker/CVE-2019-20044">CVE-2019-20044</a>
Debian Bug     : 908000 894044 894043 895225 951458</p>

<p>Several security vulnerabilities were found and corrected in zsh, a powerful
shell and scripting language. Off-by-one errors, wrong parsing of shebang lines
and buffer overflows may lead to unexpected behavior. A local, unprivileged
user can create a specially crafted message file or directory path. If the
receiving user is privileged or traverses the aforementioned path, this leads
to privilege escalation.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
5.3.1-4+deb9u4.</p>

<p>We recommend that you upgrade your zsh packages.</p>

<p>For the detailed security status of zsh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zsh">https://security-tracker.debian.org/tracker/zsh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2470.data"
# $Id: $
