<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In some configurations the MySQL storage backend for Akonadi, an
extensible cross-desktop Personal Information Management (PIM)
storage service failed to start after applying the MySQL 5.5.53
security upgrade.</p>

<p>This update extends the /etc/akonadi/mysql-global.conf configuration
file to restore compatibility (version 1.7.2-3+deb7u1 in Wheezy).</p>

<p>We recommend that you upgrade your akonadi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in akonadi version 1.7.2-3+deb7u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-710.data"
# $Id: $
