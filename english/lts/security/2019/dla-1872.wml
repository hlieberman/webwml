<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two vulnerabilities in the Django web development framework:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14232">CVE-2019-14232</a>

    <p>Prevent a possible denial-of-service in django.utils.text.Truncator.</p>

    <p>If django.utils.text.Truncator's chars() and words() methods were passed
    the html=True argument, they were extremely slow to evaluate certain inputs
    due to a catastrophic backtracking vulnerability in a regular expression.
    The chars() and words() methods are used to implement the
    truncatechars_html and truncatewords_html template filters, which were thus
    vulnerable.</p>

    <p>The regular expressions used by Truncator have been simplified in order
    to avoid potential backtracking issues. As a consequence, trailing
    punctuation may now at times be included in the truncated output.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14233">CVE-2019-14233</a>

    <p>Prevent a possible denial-of-service in strip_tags().</p>

    <p>Due to the behavior of the underlying HTMLParser,
    django.utils.html.strip_tags() would be extremely slow to evaluate certain
    inputs containing large sequences of nested incomplete HTML entities. The
    strip_tags() method is used to implement the corresponding striptags
    template filter, which was thus also vulnerable.</p>

    <p>strip_tags() now avoids recursive calls to HTMLParser when progress
    removing tags, but necessarily incomplete HTML entities, stops being
    made.</p>

    <p>Remember that absolutely NO guarantee is provided about the results of
    strip_tags() being HTML safe. So NEVER mark safe the result of a
    strip_tags() call without escaping it first, for example with
    django.utils.html.escape().</p></li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.11-1+deb8u7.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1872.data"
# $Id: $
