<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in apng2gif, a tool for converting
APNG images to animated GIF format. Improper sanitization of user input can
result in denial of service (application crash) or possible execution of
arbitrary code if a malformed image file is processed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.8-0.1~deb9u1.</p>

<p>We recommend that you upgrade your apng2gif packages.</p>

<p>For the detailed security status of apng2gif please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apng2gif">https://security-tracker.debian.org/tracker/apng2gif</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2911.data"
# $Id: $
