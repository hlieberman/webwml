<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential arbitrary code execution
vulnerability in IPython, the interactive Python shell.</p>

<p>This issue stemmed from IPython executing untrusted files in the current
working directory. According to upstream:</p>

<blockquote>
<p>
  Almost all versions of IPython looks for configuration and profiles in
  current working directory. Since IPython was developed before pip and
  environments existed, it was used a convenient way to load code/packages in a
  project dependant way.
</p>
<p>
  In 2022, it is not necessary anymore, and can lead to confusing behavior
  where for example cloning a repository and starting IPython or loading a
  notebook from any Jupyter-Compatible interface that has ipython set as a
  kernel can lead to code execution.
</p>
</blockquote>

<p>To address this problem, the current working directory is no longer searched
for profiles or configuration files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21699">CVE-2022-21699</a>

    <p>IPython (Interactive Python) is a command shell for interactive computing in multiple programming languages, originally developed for the Python programming language. Affected versions are subject to an arbitrary code execution vulnerability achieved by not properly managing cross user temporary files. This vulnerability allows one user to run code as another on the same machine. All users are advised to upgrade.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
5.1.0-3+deb9u1.</p>

<p>We recommend that you upgrade your ipython packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2896.data"
# $Id: $
