#use wml::debian::template title="[CN:支持:][HKTW:支援:]"
#use wml::debian::translation-check translation="9c90f0ed4b82c5e7504045fff4274d38f9b9997b" maintainer="Hsin-lin Cheng"
#use wml::debian::toc

# $Id$
# Translator: Franklin <franklin@goodhorse.idv.tw>, Fri Nov 15 15:11:02 CST 2002
<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian 及其[CN:支持:][HKTW:支援:]由一个由志愿者组成的\
[CN:社区:][HKTW:社群:]提供。

如果[CN:社区:][HKTW:社群:][CN:支持:][HKTW:支援:]不能满足您的需要，您可以\
阅读<a href="doc/">[CN:文档:][HKTW:文件:]</a>或者\
聘请<a href="consultants/">顾问</a>。

<toc-display/>
<toc-add-entry name="irc">IRC [CN:在線:][HKTW:線上:][CN:实时:][HKTW:即时:][CN:幫助:][HKTW:支援:]</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> 提供了一種\
讓世界各地的人們彼此進行即時交流的途徑。\
您可以在 <a href="https://www.oftc.net/">OFTC</a>
中找到屬於 Debian 的 IRC 頻道。</p>

<p>您需要[CN:通過:][HKTW:透過:]一個 IRC 客戶端才能開始使用 IRC。流行的 IRC 客戶端有：\
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>、\
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>、\
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>、\
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> 以及
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>。\
它們都已經打包成 Debian
[CN:軟件包:][HKTW:套件:]。OFTC 还提供了 <a href="https://www.oftc.net/WebChat/">WebChat</a>
界面，您可以使用浏览器连接到 IRC，而不\
需要安装任何本地客户端。</p>

<p>當您安裝好客戶端之後，您需要告訴它連到\
哪台[CN:服務器:][HKTW:伺服器:]。對于大多數的客戶端，您只要輸入</p>

<pre>
/server irc.debian.org
</pre>

<p>对于有些客户端（例如 irssi），则需要用以下的命令代替：</p>
<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>當您連上之[CN:后:][HKTW:後:]，請用以下的[CN:命令:][HKTW:指令:]加入 <code>#debian</code> 頻道：

<pre>
/join #debian
</pre>

<p>对于中文用户支持，请使用</p>

<pre>
/join #debian-zh
</pre>

<p>注意：HexChat 這一類的圖形[CN:界面:][HKTW:介面:]
IRC [CN:軟件:][HKTW:軟體:]的用法稍有不同。

<p>在這個時候您將發現自己身處在一群友好的
<code>#debian</code> [CN:用戶:][HKTW:使用者:]之中。您可以問任何關於
Debian 的問題。請參考
<url "https://wiki.debian.org/DebianIRC"> 頁面中的常見問答集。</p>


<p>也有許多其他的 IRC 網絡可以討論 Debian。</p>

<toc-add-entry name="mail_lists" href="MailingLists/">邮件列表</toc-add-entry>

<p>Debian 是世界各地的開發者通過[CN:分布:][HKTW:分散:]式的開發模式\
開發的；因此電子郵件是討論各种種事項的重要途徑。\
大部分在 Debian 開發者以及用戶之\
間的對話都是透過數個邮件列表來進行的。</p>

<p>我們有一些開放給大眾使用的邮件列表；您可以在
<a href="MailingLists/">Debian 邮件列表</a>中找到相關的[CN:信息:][HKTW:資訊:]。</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
对于中文用户支持，请联系
<a href="https://lists.debian.org/debian-chinese-big5/">debian-chinese-big5
邮件列表</a>（[HK:繁体:][TW:正体:]中文）或
<a href="https://lists.debian.org/debian-chinese-gb/">debian-chinese-gb
邮件列表</a>（简体中文）。此郵件列表歡迎任何中文或英文的文章。兩者不互通，如需完整通知信息请手动进行抄送。请务必使用 UTF-8 编码信件（而非 GB2312/GBK/GB18030/BIG5 等）。
</p>

<p>
对其他语言的用户支持，请查看\
<a href="https://lists.debian.org/users.html">面向用户的\
邮件列表索引</a>。
</p>

<p>當然還有許多其他不屬於 Debian 的邮件列表，致力於
Linux 生態系統的某些方面。使用你\
最愛的搜尋引擎找到最適合你的。
</p>


<toc-add-entry name="usenet">Usenet 新聞[CN:組:][HKTW:群組:]</toc-add-entry>

<p>我們許多的<a href="#mail_lists">邮件列表</a>可以在\
新聞[CN:組:][HKTW:群組:] <kbd>linux.debian.*</kbd> 中找到。您也可以\
利用網頁介面來瀏覽，如：\
<a href="https://groups.google.com/forum/">Google Groups</a>。

<toc-add-entry name="web">网络[CN:站點:][HKTW:站台:]</toc-add-entry>

<h3 id="forums">论坛</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Debian User Forums</a> 是一個 Debian
系統的入門论坛[CN:站點:][HKTW:網站:]。您可以在這裡提出您的問題，請求其他\
[CN:用戶:][HKTW:使用者:]協助回答。</p>

<toc-add-entry name="maintainers">[CN:联系:][HKTW:聯絡:]套件維護者</toc-add-entry>

<p>您可以[CN:通過:][HKTW:透過:]兩種不同的方式[CN:联系:][HKTW:聯絡:]套件維護者。如果您是為了\
[CN:軟件:][HKTW:軟體:]錯誤而想[CN:联系:][HKTW:聯絡:]某個維護者的話，您只需提交一份錯誤報告（bug
report）就可以了（請參考下面關於錯誤[CN:跟蹤:][HKTW:追蹤:]系統的說明），負責該\
套件的維護者自然會收到您的報告。</p>

<p>如果您只是想和該維護者溝通的話，那麼您可以使用\
為每個套件設置的特殊郵件別名。任何發送到 \
&lt;<em>套件名稱</em>&gt;@packages.debian.org 的郵件都會被轉發給\
負責維護該套件的維護者。</p>


<toc-add-entry name="bts" href="Bugs/">[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統</toc-add-entry>

<p>Debian 系統有一個對用戶和開發者所提交的[CN:軟件缺陷報告:]\
[HKTW:軟體錯誤報告:]進行歸檔管理的[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統。每個[CN:軟件缺陷報告:]\
[HKTW:軟體錯誤報告:]都被授予一個編號並且被長期[CN:跟蹤:][HKTW:追蹤:]，直到它被標記為\
已修復。</p>

<p>如果您要報告問題的話，您可以先阅读下面的網頁；我们推荐\
使用 Debian 的 <q>reportbug</q> 套件來自動提交一份[CN:缺陷:][HKTW:錯誤:]報告。</p>

<p>您可以從<a href="Bugs/">[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統頁面</a>中\
[CN:獲取:][HKTW:取得:]有關提交[CN:缺陷報告:][HKTW:錯誤報告:]、查看現有的[CN:缺陷:][HKTW:錯誤:]，\
以及有關[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統的[CN:常規:][HKTW:一般:][CN:信息:][HKTW:資訊:]。</p>


<toc-add-entry name="doc" href="doc/">[CN:文档:][HKTW:文件:]</toc-add-entry>

<p>說明[CN:文档:][HKTW:文件:][CN:与:][HKTW:與:]使用手冊在每一個[CN:操作系統:][HKTW:作業系統:]中都是很重要的一部份，是描述\
程序操作和使用的[CN:技术:][HKTW:技術:]手册。正由[CN:于:][HKTW:於:]說明[CN:文档:][HKTW:文件:]\
是[CN:創建:][HKTW:建立:]一個高品質而自由的[CN:操作系統:][HKTW:作業系統:]的重要部份，Debian 計畫花了很大的心血\
為它的[CN:用户:][HKTW:使用者:]們提供好讀而[CN:适:][HKTW:適:]當的\
說明[CN:文档:][HKTW:文件:]。</p>

<p>请查阅<a href="doc/">[CN:文档:][HKTW:文件:]页面</a>以获得 Debian 手册\
和其他[CN:文档:][HKTW:文件:]的列表，包括安装手册、Debian FAQ 以及其他\
用户和开发者手册。</p>

<toc-add-entry name="consultants" href="consultants/">顧問</toc-add-entry>

<p>Debian 是自由[CN:軟件:][HKTW:軟體:]，通過邮件列表提供免費的幫助。有些\
人由於沒有時間[CN:或者:][HKTW:或是:]由於有特別的需要，因此需要\
聘請別人來維護他們的 Debian 系統，或者是為他們的 Debian 系統\
增加新的功能。請查閱<a href="consultants/">顧問頁面</a>以獲取一份\
[CN:咨詢公司:][HKTW:顧問公司:]的列表。</p>

<toc-add-entry name="release" href="releases/stable/">已知的問題</toc-add-entry>

<p>您可以在<a href="releases/stable/">[CN:发布:][HKTW:释出:]页面</a>中找到和現在的穩定版本\
相關的技術限制和重大問題（如果有的话）。</p>

<p>請特別留意<a href="releases/stable/releasenotes">發行\
说明</a>和<a href="releases/stable/errata">錯誤修正</a>中的訊息。</p>

