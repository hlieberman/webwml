msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:10+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr ""

#: ../../english/News/news.rdf.in:19
#, fuzzy
msgid "Debian Latest News"
msgstr "Najnovejše novice"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "str. <get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr ""

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr ""

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr ""

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr ""

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr ""

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr ""

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr ""

#: ../../english/events/talks.defs:29
msgid "source"
msgstr ""

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr ""

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr ""

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr ""

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr ""

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Kmalu"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "povezava je verjetno zastarela"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Kdaj"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Kje"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Več podatkov"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Sodelovanje z Debianom"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Glavni koordinator"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Koordinator</th><th>projekta</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Sorodne povezave"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Najnovejše novice"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr ""

#: ../../english/template/debian/news.wml:9
#, fuzzy
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr "Nazaj na <a href=\"./\">svetovalce za Debian</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (mrtva povezava)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:43
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""

#: ../../english/template/debian/projectnews/footer.wml:6
#, fuzzy
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Če želite prejemati ta dopis vsak teden v poštni nabiralnik, se <a href="
"\"m4_HOME/MailingLists/subscribe#debian-news\">prijavite</a> na poštni "
"seznam debian-news."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
#, fuzzy
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
#, fuzzy
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
#, fuzzy
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
#, fuzzy
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr ""

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr ""

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr ""

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr ""

#: ../../english/template/debian/weeklynews/footer.wml:6
#, fuzzy
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Če želite prejemati ta dopis vsak teden v poštni nabiralnik, se <a href="
"\"m4_HOME/MailingLists/subscribe#debian-news\">prijavite</a> na poštni "
"seznam debian-news."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
#, fuzzy
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
#, fuzzy
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
#, fuzzy
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debianov tednik ureja <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#, fuzzy
#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Nazaj na <a href=\"./\">svetovalce za Debian</a>."

#, fuzzy
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Če želite prejemati ta dopis vsak teden v poštni nabiralnik, se <a href="
#~ "\"m4_HOME/MailingLists/subscribe#debian-news\">prijavite</a> na poštni "
#~ "seznam debian-news."
