#use wml::debian::cdimage title="Debian CD/DVD képek letöltése HTTP/FTP-n keresztül" BARETITLE=true
#use wml::debian::translation-check translation="0df681afe3c2fe6da170ce34a99d4f736206f770" maintainer="Szabolcs Siebenhofer"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p><strong>
A CD vagy DVD képeket ne úgy töltsd le, mint a többi fájlt, a böngésződdel.
</strong> Ennek az az oka, hogy a legtöbb böngésző nem tudja folytatni attól a ponttól 
a letöltést, ahol esetleg az megszakadt.</p>
</div>

<p>Ehelyett, használj olyan programot, amit támogatja a folytatást - általában ezeket <q>dowload manager-nek (letöltés kezelőnek)</q> hívják. Nagyonsok böngésző kiegészítő van erre, vagy ha gondolod, külön programot is telepíthetsz. Linux/Unix alatt például
használhatod az <a href="http://aria2.sourceforge.net/">aria2</a>-t, <a href="http://dfast.sourceforge.net/">wxDownload Fast-ot</a> vagy (parancssorból)
<q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> or
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>. Szintén nézz szét a <a href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">
letöltés vezérlők összehasonlító oldalán</a>.</p>

<p>A következő Debian képek tölthetőek le:</p>

<ul>

  <li><a href="#stable">Hivatalos CD/DVD képek a <q>stabil</q> kiadáshoz</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Hivatalos
  CD/DVD képek a <q>testing</q> terjesztéshez (<em>hetente frissítve</em>)</a></li>

<comment>
  <li>Unofficial CD/DVD images of the <q>testing</q> and <q>unstable</q>
  distributions by fsn://HU &mdash; <a href="#unofficial">see below</a></li>
</comment>

</ul>

<p>Lásd még:</p>
<ul>

  <li>A <a href="#mirrors"><tt>debian-cd/</tt> tükrözések</a> teljes listája</li>

  <li>A <q>network install</q> (150-300&nbsp;MB) képekkel kapcsolatosan lásd a 
  <a href="../netinst/">hálózati telepítést</a> oldalt.</li>

  <li>A <q>testing</q> kiadás <q>netinst</q> napi frissítésű képeivel
  kapcsolatban (ismert még működő pillanatképként) lásd a <a
  href="$(DEVEL)/debian-installer/">Debian-Installer oldalt</a>.</li>

</ul>

<hr />

<h2><a name="stable">Hivatalos CD/DVD képek a <q>stabil</q> kiadáshoz</a></h2>

<p>Ha olyan gépen kell Debiant telepíteni, amelyiken nem áll rendelkezésre internet kapcsolat,
lehetséges CD (700&nbsp;MB) vagy DVD (4,4&nbsp;GB) képek használata. Először töltsd le az első  
CD/DVD képet, majd egy CD/DVD író segítségével írd ki, majd indítsd újra a gépet arról.</p>

<p><strong>Az első</strong> CD/DVD lemez minden szükséges fájlt tartalmaz egy sztenderd
Debian rendszer telepítéséhez.<br />
A felesleges letöltések elkerülése maitt, kérjük <strong>ne</strong> tölts le más CD vagy DVD 
képet, míg nem tudod, hogy melyikre van szükséged.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>A következő linkek olyan képekre mutatnak, melyek legfeljebb 650&nbsp;MB méretűek,
és úgy készültek, hogy normál CD-re lehessen írni azokat:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>A következő linkek olyan képekre mutatnak, melyek legfeljebb 4,4&nbsp;GB méretűek,
és úgy készültek, hogy normál DVD-R/DVD+R vagy hasonló médiára lehessen írni azokat:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>A telepítés megkezdése előtt vess egy pillantást a dokumentációra. <strong>
Ha csak egy dokumentumot akarsz elolvasni</strong> a telepítés előtt, akkor
olvasd el a <a href="$(HOME)/releases/stable/i386/apa">Telepítés HOGYAN-t</a>,
amely egy gyors áttekintés a telepítés folyamatáról. Haszons információkat találhatsz még:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Telepítési útmutató</a>,
    részletes telepítési instrukciók</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    Dokumentáció</a>, tartalmazza a GYIK-ot, gyakori kérdésekkel és válaszokkal</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Hibajegyzék</a>, a telepítő ismert hibáinak jegyzéke</li>
</ul>

<hr />

<h2><a name="mirrors">A <q>debian-cd</q> archívum nyilvántartott tükrözései</a></h2>

<p>Ne feledd, <strong>néhány tükrözés nem naprakész</strong> &mdash; mielőtt
letöltenél, ellenőrízd a kép verziószámát, hogy az megegyezik-e az <a href="../#latest">itt találhatóval</a>! Továbbá, néhány oldal nem a teljes készletet tükrözi 
(különösen a DVD képeket), a méretük miatt.</p>

<p><strong>Ha kétséged támadna, használd az <a href="https://cdimage.debian.org/debian-cd/">elsődleges CD kép szervert</a> Svédországból</strong> vagy próbáld ki a 
<a href="http://debian-cd.debian.net/">kísérleti automatikus tükör választót</a> ami 
átirányít majd a legközelebbi tükörszerverre, amiről tudni, hogy a jelenlegi verzió elérhető.</p>

<p>Szeretnéd Te is kínálni a Debian CD képeket a tükrözéseden? Ha igen,
nézd meg, <a href="../mirroring/">hogyan kell beállítani
egy CD kép türközést mirror</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"


<comment>
<h2><a name="unofficial">Unofficial CD/DVD images of the <q>testing</q> and
<q>unstable</q> releases</a></h2>

<p>These images are not built and offered by Debian, but by <a
href="http://www.fsn.hu/">fsn://HU</a>:</p>

<ul>

  <li>fsn://HU images for the <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/etch/">\
  <q>testing</q> distribution</a> on CD (<em>amd64 and i386, regenerated
  weekly</em>), <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  mirrors</a></li>

  <li>fsn://HU images for the <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/etch-dvd/">\
  <q>testing</q> distribution</a> on DVD (<em>amd64 and i386, regenerated
  weekly</em>), <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  mirrors</a></li>

  <li>fsn://HU images for the <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/sid/">\
  <q>unstable</q> distribution</a> on CD (<em>amd64 and i386, regenerated
  weekly</em>), <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  mirrors</a></li>

  <li>fsn://HU images for the <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/sid-dvd/">\
  <q>unstable</q> distribution</a> on DVD (<em>amd64 and i386, regenerated
  weekly</em>), <a
  href="ftp://ftp.fsn.hu/pub/CDROM-Images/debian-unofficial/MIRRORS">\
  mirrors</a></li>

</ul>
</comment>
