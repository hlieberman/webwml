#use wml::debian::template title="지원"
#use wml::debian::toc
#use wml::debian::translation-check translation="9c90f0ed4b82c5e7504045fff4274d38f9b9997b" maintainer="Sebul"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

데비안과 데비안 지원은 자원봉사자 커뮤니티에 의해 운영됩니다.

여러분이 필요로 하는 것이 이러한 커뮤니티 중심의 지원으로 충족되지 않는다면,
<a href="doc/">문서</a>를 읽어보거나
<a href="consultants/">컨설턴트</a>를 고용하세요.


<toc-display />

<toc-add-entry name="irc">IRC를 이용한 실시간 온라인 지원</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC(Internet Relay Chat)</a>는 전세계 사람들과
실시간으로 채팅할 수 있는 수단입니다.
데비안에 할당된 IRC 채널은 <a href="https://www.oftc.net/">OFTC</a>에서
볼 수 있습니다.</p>

<p>여러분이 IRC에 연결하려면 IRC 클라이언트가 필요합니다. 몇몇 아주 유명한
클라이언트는
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a>,
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>가 있고,
모두 데비안에 패키지로 들어있습니다.

OFTC는 클라이언트를 설치하지 않고도 웹브라우저를 통해 IRC에 연결할 수 있는
웹 인터페이스도 제공하는데,
<a href="https://www.oftc.net/WebChat/">WebChat</a>이 바로 그것이죠.</p>

<p>클라이언트를 설치하고 나면 여러분은 클라이언트가 서버로 연결되도록 해야 합니다.
대부분 클라이언트에서는 여러분이 직접 아래처럼 입력을 해야 하죠:</p>

<pre>
/server irc.debian.org
</pre>

<p>irssi와 같은 몇몇 클라이언트는 아래처럼 입력해야 할 겁니다:
</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>일단 연결되고 나면, 아래처럼 입력해서 <code>#debian</code> 채널에 가입하세요.</p>

<pre>
/join #debian
</pre>

<p>주의: HexChat과 같은 클라이언트는 종종 서버나 채널에 가입하기 위해 다른
그래픽 유저 인터페이스(GUI)를 사용합니다.</p>

<p>가입했을 때 친절한 <code>#debian</code> 주민들 사이에 있는 여러분을 발견하게
될 겁니다.
여러분이 데비안에 대해 질문하는 것은 언제나 환영이죠.
채널과 관련된 자주 묻는 질문(FAQ)은 <url "https://wiki.debian.org/DebianIRC" />에
있습니다.</p>


<p>여러분이 데비안에 대해서 이야기할 수 있는 수많은 IRC 네트워크도 있습니다.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">메일링 리스트</toc-add-entry>

<p>데비안은 분산되어 전세계에서 개발됩니다. 따라서, 이메일은 다양한
항목을 논의하는데 선호하는 방법이죠.
데비안 개발자와 유저들 사이의 대화는 많은 부분이 서버의 메일링 리스트를
통해 관리됩니다.</p>

<p>공개된 메일링 리스트가 있는데, 더 많은 정보는
<a href="MailingLists/">데비안 메일링 리스트</a> 페이지에 있습니다.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>영어로 된 지원에 대해서는,
<a href="https://lists.debian.org/debian-user/">데비안 사용자 메일링 리스트</a>에
연락하세요.</p>

<p>영어 이외에 다른 언어로 된 지원에 대해서는,
<a href="https://lists.debian.org/users.html">사용자 메일링 리스트 색인</a>을 보세요.</p>

<p>물론, 광대한 리눅스 생태계의 어떤 면에 특화되긴 했지만, 데비안에는 구체적이지
않은 수많은 메일링 리스트도 있습니다.
좋아하는 검색 엔진으로 여러분의 목적에 맞는 리스트를 찾으세요.</p>

<toc-add-entry name="usenet">유즈넷 (Usenet) 뉴스 그룹</toc-add-entry>

<p>뉴스 그룹을 통해 <kbd>linux.debian.*</kbd> 계층의 수많은
<a href="#mail_lists">메일링 리스트</a>를 찾을 수 있습니다.
<a href="https://groups.google.com/forum/">구글 그룹</a> 등을
통해 읽을 수도 있죠.</p>

<toc-add-entry name="web">웹 사이트</toc-add-entry>

<h3 id="forums">포럼</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a
href="http://forums.debian.net">데비안 사용자 포럼</a>은
데비안 관련 주제를 토론하고, 데비안에 대한 질문을 제출하고, 다른 사용자가 답할 수 있는
웹 포털입니다.</p>

<toc-add-entry name="maintainers">패키지 관리자에게 연락하기</toc-add-entry>

<p>패키지 관리자에게 연락하는 방법이 두 가지 있습니다.
여러분이 버그 때문에 관리자에게 연락하려고 한다면, 간단히
버그 리포트를 제출하세요. 버그 리포트에 대해서는 아래의버그 추적
시스템 절을 보세요.
패키지 관리자는 버그 리포트 사본을 받을 겁니다.</p>

<p>단지 관리자와 소통하고 싶다면, 여러분은 각 패키지마다 특별한 메일
별칭을 사용할 수 있습니다.
&lt;<em>패키지 이름</em>&gt;@packages.debian.org에 보내진 메일은 해당 패키지를
책임지고 있는 관리자에게 전달될 겁니다.</p>

<toc-add-entry name="bts" href="Bugs/">버그 추적 시스템</toc-add-entry>

<p>데비안 배포판에는 사용자와 개발자가 보고한 버그를 자세히 기록하는
버그 추적 시스템이 있습니다.
각 버그에는 번호가 붙고, 처리된 것으로 표시될 때까지 파일에 남습니다.</p>

<p>버그를 보고하기 위해 아래에 있는 버그 페이지를 볼 수 있습니다.
단, 우리는 데비안 패키지 중 <q>reportbug</q>를 써서 버그 리포트를 자동으로 하기를
권합니다.</p>

<p>버그 제출, 현재 활성화된 버그 보기, 버그 추적 시스템의 일반적인 내용에 대한 정보는
<a href="Bugs/">버그 추적 시스템 웹 페이지</a>에서
볼 수 있습니다.</p>

<toc-add-entry name="doc" href="doc/">문서</toc-add-entry>

<p>모든 운영 체제의 중요한 부분은 프로그램의 동작과 사용을 설명하는 기술 설명서입니다. 
고품질 무료 운영 체제를 만들기 위한 노력의 일환으로 
데비안 프로젝트는 모든 사용자에게 접근하기 쉬운 형태로 적절한 문서를 제공하기 위해 모든 노력을 기울이고 있습니다.</p>

<p>데비안 설명서 및 설치 안내서, 데비안 FAQ 및 기타 사용자 및 개발자 설명서를 포함한 기타 설명서 목록은 
<a href="doc/">설명서 페이지</a>를 참조하십시오.</p>

<toc-add-entry name="consultants" href="consultants/">컨설턴트</toc-add-entry>

<p>데비안은 자유 소프트웨어이며 메일링 리스트를 통해 자유롭게
도움을 줍니다.
어떤 사람들은 시간이 없거나 특별한 필요가 있으며, 누군가를 고용하여 데비안
시스템을 유지하고 기능을 추가하길 원할 수 있습니다.
이런 사람이나 회사는 <a href="consultants/">컨설턴트 페이지</a>를
보세요.</p>

<toc-add-entry name="release" href="releases/stable/">알려진 문제</toc-add-entry>

<p>현재 안정 배포판에 제약사항이나 심각한 문제가 있다면,
<a href="releases/stable/">릴리스 페이지</a>에 기록됩니다.</p>

<p><a href="releases/stable/releasenotes">릴리스 노트</a>와
<a href="releases/stable/errata">정오표</a>를 주의 깊게 보세요.</p>
