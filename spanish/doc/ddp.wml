#use wml::debian::ddp title="Proyecto de documentación de Debian (DDP)"
#use wml::debian::translation-check translation="e8de3f9543c6c16d99765f9d2b60925bd6495dd0"

<p>El Proyecto de Documentación de Debian (DDP) se formó para coordinar y unificar 
todos los esfuerzos y escribir más y mejor documentación para el sistema Debian.</p>

  <h2>Trabajo del DDP</h2>
<div class="line">
  <div class="item col50">

    <h3>Manuales</h3>
    <ul>
      <li><strong><a href="user-manuals">Manuales de usuario</a></strong></li>
      <li><strong><a href="devel-manuals">Manuales de desarrollador</a></strong></li>
      <li><strong><a href="misc-manuals">Manuales varios</a></strong></li>
      <li><strong><a href="#other">Manuales problemáticos</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Normativa de la documentación</h3>
    <ul>
      <li>La licencia de los manuales ha de cumplir las DFSG.</li>
      <li>Usamos Docbook XML para nuestros documentos.</li>
      <li>Los fuentes deberían residir en <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a>.</li>
      <li>La URL oficial será <tt>www.debian.org/doc/&lt;nombre-del-manual&gt;</tt>.</li>
      <li>Todos los documentos deberían mantenerse actualizados.</li>
      <li>Pregunte en <a href="https://lists.debian.org/debian-doc/">debian-doc</a> si quiere escribir un nuevo documento.</li>
    </ul>

    <h3>Acceso mediante Git</h3>
    <ul>
      <li><a href="vcs">Cómo acceder</a> a los repositorios git del DDP.</li>
    </ul>

  </div>


</div>

<hr />

<h2><a name="other">Manuales problemáticos</a></h2>

<p>Además de los manuales normalmente anunciados, mantenemos los siguientes
manuales, que son problemáticos de uno u otro modo, por lo que no podemos recomendárselos 
a todos los usuarios. Caveat emptor (N. del T.: «Tenga cuidado» en latín)</p>

<ul>
  <li><a href="obsolete#tutorial">Tutorial de Debian</a>, obsoleto.</li>
  <li><a href="obsolete#guide">Guía de Debian</a>, obsoleta.</li>
  <li><a href="obsolete#userref">Manual de Referencia de Usuario de Debian</a>,
      parado y bastante incompleto.</li>
  <li><a href="obsolete#system">Manual del administrador del sistema de Debian</a>, parado, casi vacío.</li>
  <li><a href="obsolete#network">Manual del administrador de redes de Debian</a>, parado, incompleto.</li>
  <li><a href="obsolete#swprod">Cómo los productores de «software» pueden distribuir sus productos directamente en formato .deb</a>, parado, anticuado.</li>
  <li><a href="obsolete#packman">Manual de empaquetado de Debian</a>, 
      incluido parcialmente en el <a href="devel-manuals#policy">
      Manual de normativas de Debian</a>. El resto se incluirá en el manual de referencia 
      de dpkg que aún está por escribir.</li>
  <li><a href="devel-manuals#makeadeb">Introducción: Hacer un paquete Debian
      </a>, sustituido por la
      <a href="devel-manuals#maint-guide">Guía del nuevo desarrollador de Debian</a>.</li>
  <li><a href="obsolete#programmers">Manual para programadores de Debian</a>,
      sustituido por la
      <a href="devel-manuals#maint-guide">Guía del nuevo desarrollador de Debian</a>
      y la
      <a href="devel-manuals#debmake-doc">Guía para Mantenedores de Debian</a>.</li>
  <li><a href="obsolete#repo">Repositorio HOW TO de Debian</a>, obsoleto tras la introducción de los procesos de seguridad criptográfica de Apt.</li>
  <li><a href="obsolete#i18n">Introducción a la i18n</a>, parado.</li>
  <li><a href="obsolete#sgml-howto">Cómo de Debian sobre SGML/XML («Debian SGML/XML HOWTO»)</a>, parado, obsoleto.</li>
  <li><a href="obsolete#markup">Manual de marcas de Debiandoc-SGML («Debiandoc-SGML Markup Manual»)</a>, parado; DebianDoc está en proceso de ser eliminado.</li>
</ul>
