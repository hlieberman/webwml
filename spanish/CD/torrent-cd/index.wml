#use wml::debian::cdimage title="Descarga de imágenes de CD de Debian usando BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

<p><a href="https://es.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
es un sistema de descarga P2P optimizado para gran cantidad
de descargas simultáneas. Impone una carga mínima en nuestros 
servidores, porque los clientes de BitTorrent envían partes del 
ficheros a otros usuarios durante la descarga, con lo cual 
reparte la carga por la red y consigue que las posibles 
descargas se hagan muy rápido.
</p>

<div class="tip">
<p>El <strong>primer</strong> disco CD/DVD contiene todos los archivos
necesarios para instalar un sistema estándar de Debian.<br />
Para evitar descargas innecesarias, por favor, <strong>no</strong> descargue
otras imágenes de CD o DVD a menos que sepa que necesita los paquetes que
contienen.</p>
</div>

<p>
Necesitará un cliente de BitTorrent para descargar así las imágenes de CD/DVD 
de Debian. En la distribución de Debian, estos son
<a href="https://packages.debian.org/bittornado">BitTornado</a>,
<a href="https://packages.debian.org/ktorrent">KTorrent</a> y las herramientas  
<a href="https://packages.debian.org/bittorrent">BitTorrent</a> originales.
Los que funcionan en otros sistemas operativos son
<a href="http://www.bittornado.com/download.html">BitTornado</a> y <a
href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>

  <h3>Ficheros torrent oficiales de la publicación <q>estable</q></h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Asegúrese de echarle un vistazo a la documentación antes de instalar.
<strong>Si sólo va a leer un documento</strong> antes de instalar, lea nuestro
<a href="$(HOME)/releases/stable/i386/apa">Cómo instalar</a>, un paseo rápido
por el proceso de instalación. Entre otra documentación útil están:
</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guía de instalación</a>,
    las instrucciones de instalación detalladas.</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentación del instalador de Debian</a>,
incluyendo las respuestas a preguntas frecuentes.</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Erratas del instalador de Debian</a>,
    la lista de problemas conocidos en el instalador.</li>
 </ul>

#<h3>Ficheros torrents oficiales de la distribución en «pruebas»</h3>
#<ul>
#	<li><strong>CD</strong>:<br />
#	<full-cd-torrent>
#	</li>
#
#	<li><strong>DVD</strong>:<br />
#	<full-dvd-torrent>
#	</li>
#</ul>

<p>
Por favor, si es posible, deje funcionando su cliente tras completar
la descarga, para ayudar a que otros descarguen más rápido sus imágenes.
</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Si algún componente hardware de su sistema <strong>requiere cargar «firmware»
no libre</strong> con el controlador de dispositivo, puede usar uno de los
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archivos comprimidos con paquetes de «firmware» común</a> o descargar una imagen <strong>no oficial</strong>
que incluya estos «firmwares» <strong>no libres</strong>. En la <a href="../../releases/stable/amd64/ch06s04">guía de instalación</a> puede encontrar
instrucciones sobre cómo usar los archivos comprimidos e información general sobre cómo cargar
el «firmware» durante la instalación.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imágenes
no oficiales de instalación de <q>estable</q> con «firmware» incluido</a>
</p>
</div>
