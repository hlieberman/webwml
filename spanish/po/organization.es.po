# Debian website translation - organization
# Copyright (C) 2001-2005 SPI, Inc.
#
# Translator: Javier Fernández-Sanguino <jfs@debian.org>, 2004-2011
#
#
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-09-26 22:45+0200\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "correo de delegación"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "correo de nombramiento"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegado"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>él (he/him en inglés)"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>ella (she/her en inglés)"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegade"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>elle (they/them en inglés)"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "miembro"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "gestor"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM (resp. public. vers. estable)"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Responsable de publicación de la versión estable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mago"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "presidente"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asistente"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretario"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "representante"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "rol"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"En la siguiente lista, <q>actual</q> se usa para puestos que son\n"
"temporales (elegidos o propuestos con una fecha de caducidad determinada)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Directores"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribución"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Comunicación y extensión"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Equipo de protección de datos"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Equipo de publicidad"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Membresía en otras organizaciones"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:302
msgid "Support and Infrastructure"
msgstr "Apoyo e infrastructura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comité técnico"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Secretario"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Proyectos de desarrollo"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Archivo FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Responsables del FTP"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Ayudantes de FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Magos del FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Equipo de adaptaciones a estable («backports»)"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Gestión de la publicación de versiones"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Equipo responsable de la publicación"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Garantía de calidad"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Equipo del sistema de instalación"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr "Equipo de Debian «en vivo»"

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Notas de la publicación"

#: ../../english/intro/organization.data:153
msgid "CD/DVD/USB Images"
msgstr "Imágenes de CD/DVD/USB"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Producción"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Pruebas"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Equipo para la nube"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Infrastructura de autocompiladores"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Equipo de wanna-build"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Administración de buildd"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Documentación"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista de paquetes en perspectiva o en los que se necesita ayuda"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Contacto de prensa"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Páginas web"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Extensión"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Proyecto Mujeres Debian"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Comunidad"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Para enviar un mensaje privado a todos los miembros del equipo de Comunidad, "
"use la clave GPG <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Eventos"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "Comité de la DebConf"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Programa de Socios"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Coordinación de donaciones de hardware"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "Fundación GNOME"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Base Estándar para Linux"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Grupo de estándares libres"

#: ../../english/intro/organization.data:292
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organización\n"
"     para el Avance de Estándares de Información Estructurada"

#: ../../english/intro/organization.data:295
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:298
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:305
msgid "Bug Tracking System"
msgstr "Sistema de seguimiento de errores"

#: ../../english/intro/organization.data:310
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administración de las listas de correo y de los archivos de las listas"

#: ../../english/intro/organization.data:318
msgid "New Members Front Desk"
msgstr "Recepción de nuevos miembros"

#: ../../english/intro/organization.data:324
msgid "Debian Account Managers"
msgstr "Gestores de cuentas de Debian"

#: ../../english/intro/organization.data:328
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Para enviar un mensaje privado a todos los Gestores de cuentas de Debian, "
"use la clave GPG 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:329
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Responsable del anillo de claves (PGP y GPG)"

#: ../../english/intro/organization.data:333
msgid "Security Team"
msgstr "Equipo de seguridad"

#: ../../english/intro/organization.data:346
msgid "Policy"
msgstr "Normativa"

#: ../../english/intro/organization.data:349
msgid "System Administration"
msgstr "Administración del sistema"

#: ../../english/intro/organization.data:350
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Esta es la dirección a usar en caso de encontrar problemas en alguna de las "
"máquinas de Debian, incluyendo problemas con las contraseñas o si necesita "
"que se instale algún paquete."

#: ../../english/intro/organization.data:359
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Si tiene problemas con las máquinas de Debian, por favor, vea la página de "
"<a href=\"https://db.debian.org/machines.cgi\">Máquinas de Debian</a>, que "
"debería contener información del administrador de cada máquina."

#: ../../english/intro/organization.data:360
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador del directorio LDAP de desarrolladores"

#: ../../english/intro/organization.data:361
msgid "Mirrors"
msgstr "Réplicas"

#: ../../english/intro/organization.data:364
msgid "DNS Maintainer"
msgstr "Responsable del DNS"

#: ../../english/intro/organization.data:365
msgid "Package Tracking System"
msgstr "Sistema de seguimiento de paquetes"

#: ../../english/intro/organization.data:367
msgid "Treasurer"
msgstr "Tesorería"

#: ../../english/intro/organization.data:374
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Solicitudes de uso de la <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Marca Registrada</a>"

#: ../../english/intro/organization.data:378
msgid "Salsa administrators"
msgstr "Administradores de Salsa"

#~ msgid "APT Team"
#~ msgstr "Equipo de APT"

#~ msgid "Accountant"
#~ msgstr "Contabilidad"

#~ msgid "Alioth administrators"
#~ msgstr "Administradores de Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Anti acoso"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Bits from Debian"
#~ msgstr "Bits de Debian"

#~ msgid "CD Vendors Page"
#~ msgstr "Página de vendedores de CD"

#~ msgid "Consultants Page"
#~ msgstr "Página de consultores"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribuciones Debian Derivadas"

#~ msgid "DebConf chairs"
#~ msgstr "organizadores de Debconf"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux para el cómputo empresarial"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Grupo de gestión del anillo de claves de los desarrolladores Debian"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribución multimedia Debian"

#~ msgid "Debian Pure Blends"
#~ msgstr "Mezclas puras de Debian («Pure Blends»)"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian para la astronomía"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian para niños de 1 a 99"

#~ msgid "Debian for education"
#~ msgstr "Debian para la educación"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian para investigación y práctica médica"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian para asociaciones sin ánimo de lucro"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian para personas con discapacidades"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian para la ciencia e investigación relacionada"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian en oficinas de abogados"

#~ msgid "Delegates"
#~ msgstr "Delegados"

#~ msgid "Embedded systems"
#~ msgstr "Sistemas embebidos"

#~ msgid "Firewalls"
#~ msgstr "Cortafuegos"

#~ msgid "Handhelds"
#~ msgstr "Agendas electrónicas y similares"

#~ msgid "Individual Packages"
#~ msgstr "Paquetes individuales"

#~ msgid "Installation"
#~ msgstr "Instalación"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Sistema de instalación de la versión «estable»"

#~ msgid "Internal Projects"
#~ msgstr "Proyectos internos"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordinación de firma de claves"

#~ msgid "Laptops"
#~ msgstr "Portátiles"

#~ msgid "Live System Team"
#~ msgstr "Equipo del sistema de instalación «live»"

#~ msgid "Mailing List Archives"
#~ msgstr "Archivos de las listas de correo"

#~ msgid "Mailing list"
#~ msgstr "Lista de correo"

#~ msgid "Marketing Team"
#~ msgstr "Equipo de marketing"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Puede encontrar los nombres de los administradores de un buildd "
#~ "particular en <a href=\"http://www.buildd.net\">http://www.buildd.net</"
#~ "a>. Escoja una arquitectura y distribución para ver la lista de máquinas "
#~ "buildd disponibles y sus administradores."

#~ msgid "Ports"
#~ msgstr "Adaptaciones"

#~ msgid "Publicity"
#~ msgstr "Publicidad"

#~ msgid "Release Assistants"
#~ msgstr "Asistentes a la publicación de versiones"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Asistentes a la publicación de «estable»"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Equipo responsable de la publicación de «estable»"

# JFS: Seguramente haya una mejor traducción...
#~ msgid "Release Wizard"
#~ msgstr "Mago de la publicación"

#~ msgid "SchoolForge"
#~ msgstr "SchoolForge"

#~ msgid "Security Audit Project"
#~ msgstr "Proyecto de auditoría de seguridad"

#~ msgid "Special Configurations"
#~ msgstr "Configuraciones especiales"

#~ msgid "Testing Security Team"
#~ msgstr "Equipo de seguridad de la distribución «en pruebas»"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "El sistema operativo universal como su escritorio"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Puede contactar con los administadores responsables para los buildd de "
#~ "una arquitectura específica en la dirección <genericemail arch@buildd."
#~ "debian.org>. Por ejemplo: <genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Éste no es aún un proyecto interno oficial de Debian pero ha anunciado su "
#~ "intención de integrarse."

#~ msgid "User support"
#~ msgstr "Soporte a usuarios"

#~ msgid "Vendors"
#~ msgstr "Vendedores"

#~ msgid "Volatile Team"
#~ msgstr "Equipo responsable de «volatile»"

#~ msgid "current Debian Project Leader"
#~ msgstr "Líder del Proyecto Debian actual"
