#use wml::debian::template title="Informationen zur Debian-<q>Buster</q>-Veröffentlichung"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f"

<p>Debian <current_release_buster> wurde am <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>
veröffentlicht.
<ifneq "10.0" "<current_release>"
"(Debian 10.0 wurde ursprünglich am <:=spokendate('2019-07-06'):> freigegeben.)"
/>
Diese Veröffentlichung enthält größere Änderungen, die in
unserer <a href="$(HOME)/News/2019/20190706">Pressemitteilung</a> und
in den <a href="releasenotes">Veröffentlichungshinweisen</a> beschrieben
sind.</p>

<p><strong>Debian 10 wurde durch
<a href="../bullseye/">Debian 11 (<q>Bullseye</q>)</a> abgelöst.
#Sicherheitsaktualisierungen wurden am <:=spokendate('xxxx-xx-xx'):> eingestellt.
</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Buster profitiert jedoch zusätzlich bis Ende Juni 2024 vom Long Term Support (LTS), d. h.
#bis zu diesem Zeitpunkt werden Sicherheits-Updates für Buster bereitgestellt.
#Dies ist allerdings beschränkt auf i386, amd64, armel, armhf und arm64. Weitere Informationen
#hierzu finden Sie im <a
#href="https://wiki.debian.org/LTS">LTS-Abschnitt des Debian-Wikis</a>.
#</strong></p>

<p>Um Debian zu beschaffen und zu installieren, lesen Sie die
<a href="debian-installer/">Webseite zum Debian-Installer</a> und die <a
href="installmanual">Installationsanleitung</a>. Wenn Sie ein Upgrade von einer
älteren Debian-Veröffentlichung durchführen möchten, lesen Sie die Anleitung in den
<a href="releasenotes">Veröffentlichungshinweisen</a>.</p>

### Activate the following when LTS period starts.
#<p>Während der Long-Term-Support-Periode unterstützte Architekturen:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Zur ursprünglichen Freigabe von Buster unterstützte Architekturen:</p>

<ul>
<:
foreach $arch (@arches) {
print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Entgegen unseren Wünschen könnte es in der Veröffentlichung noch einige
Probleme geben, obwohl sie als <em>stabil</em> deklariert
wurde. Wir haben <a href="errata">eine Liste bekannter größerer Probleme</a>
erstellt und Sie können uns jederzeit <a href="reportingbugs">weitere
Probleme berichten</a>.</p>

<p>Zu guter Letzt finden Sie hier auch <a href="credits">eine Liste der Personen,
denen Dank dafür gebührt</a>, dass diese Veröffentlichung erfolgen konnte.</p>
