#use wml::debian::template title="Informasjon om Debian 2.1 (slink)" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/slink/formats_and_architectures.wmh"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd" maintainer="Hans F. Nordhaug"

<ul>
  <li><a href="#release-notes">Utgivelsesmerknader</a></li>
  <li><a href="#new-inst">Nye installasjoner</a></li>
  <li><a href="#errata">Feil</a></li>
</ul>

<p><strong>Debian 2.1 er erstattet av nyere versjoner.</strong></p>

<p>Følgende datamaskinarkitekturer var støttet i denne utgaven:</p>

<ul>
<: foreach $arch (@arches) {
      print "<li> " . $arches{$arch} . "\n";
   } :>
</ul>


<h2><a name="release-notes"></a>Utgivelsesmerknader</h2>

<p>For at finde ud af hvad der er af nyt i Debian 2.1, kan du kigge på 
udgivelsesbemærkningerne til din arkitektur.  Udgivelsesbemærkningerne
indeholder også instruktioner til brugere der opgraderer fra tidligere 
udgivelser.</p>

<ul>
<: &permute_as_list('release-notes/', 'Udgivelesbemærkninger'); :>
</ul>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arkitektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Sprog</strong></th>
</tr>
<: &permute_as_matrix('release-notes', 'english', 'croatian', 'czech',
		      'japanese', 'portuguese', 'russian');
:>
</table>
</div>

<p>Der findes en <a href="i386/reports">detaljeret rapport</a> vedrørende 
i386-arkitekturen, som beskriver pakker der er ændret siden de seneste to
udgivelser.</p>


<h2><a name="new-inst"></a>Nye installasjoner</h2>

<p>Installeringsveiledning, foruden filer til download, er opdelt efter 
arkitektur:</p>

<ul>
<:= &permute_as_list('install', 'Installeringsveiledning'); :>
</ul>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arkitektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Sprog</strong></th>
</tr>
<:= &permute_as_matrix('install', 'english', 'croatian', 'czech', 
		      'finnish', 'french', 
		      'japanese', 'portuguese', 'russian', 'spanish');
:>
</table>
</div>

<h2><a name="errata"></a>Feil</h2>

<p>I tilfælde af kritiske problemer eller sikkerhedsopdateringer, bliver den 
udgivne distribution (Slink, i dette tilfælde) opdateret. Generelt angives 
dette som punktopdateringer.  Den aktuelle punktopdatering er Debian 2.1r5.  
Du kan se <a href="http://archive.debian.org/debian/dists/slink/ChangeLog">\
ChangeLog</a> på alle Debians arkivspeile.</p>

<p>Rettelser til den udgivene, stabile distribution gennemgår ofte en udvidet 
testperiode, før de accepteres i arkivet. Men disse rettelser er tilgængelige i
<a href="http://archive.debian.org/debian/dists/slink-proposed-updates/">mappen
dists/slink-proposed-updates</a> i alle Debians arkivspeile.
Hvis du bruger <tt>apt</tt> til at opdatere pakker, kan du installere 
foreslåede opdateringer ved at tilføje den følgende linie til 
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  deb http://archive.debian.org dists/slink-proposed-updates/
</pre>

<p>Kør dernæst <kbd>apt-get update; apt-get upgrade</kbd>.</p>

<p>Slink er certificeret til brug med 2.0.x-serien af Linux-kerner. Hvis du 
ønsker at køre Linux 2.2.x-kernen med slink, se 
<a href="running-kernel-2.2">listen over kendte problemer</a>.</p>
