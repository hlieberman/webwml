#use wml::debian::template title="Raisons pour choisir Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Jean-Pierre Giraud"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian pour les utilisateurs</a></li>
    <li><a href="#devel">Debian pour les développeurs</a></li>
    <li><a href="#enterprise">Debian dans un environnement d'entreprise</a></li>
  </ul>
</div>

<p>
Il existe beaucoup de raisons pour lesquelles Debian est choisie comme système
d'exploitation par les utilisateurs, les développeurs et même dans un
environnement d'entreprise. La plupart des utilisateurs apprécient sa stabilité
et la douceur des processus de mise à niveau aussi bien des paquets que de la
distribution dans son entier. Debian est aussi très largement utilisée par les
développeurs de logiciels et de matériels parce qu'elle fonctionne sur de
nombreux périphériques et architectures, et elle fournit un système de suivi de
bogues public ainsi que d'autres outils pour les développeurs. Si vous projetez
d'utiliser Debian dans un environnement professionnel, il existe d'autres
avantages comme les versions avec suivi à long terme et les images pour
l'informatique dématérialisée.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>Pour moi, Debian est à un niveau parfait de facilité d'utilisation et de stabilité. J'ai utilisé au fil des ans plusieurs distributions différentes, mais Debian est la seule qui fonctionne, tout simplement. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest sur Reddit</a></p>
</aside>

<h2><a id="users">Debian pour les utilisateurs</a></h2>

<dl>
  <dt><strong>Debian est un logiciel libre.</strong></dt>
  <dd>
    Debian est une distribution faite de logiciels libres et au code source
    ouvert et sera toujours 100 % <a href="free">libre</a> — libre à chacun
    de l'utiliser, de la modifier et de la distribuer. C'est notre engagement
    principal vis à vis de <a href="../users">nos utilisateurs</a>. Elle est
    aussi gratuite.
  </dd>
</dl>

<dl>
  <dt><strong>Debian est un système d'exploitation stable et sûr.</strong></dt>
  <dd>
    Debian est un système d'exploitation basé sur Linux pour une large gamme
    d'appareils depuis les ordinateurs portables jusqu'aux ordinateurs de bureau
    et aux serveurs. Elle fournit une configuration par défaut correcte pour
    chaque paquet ainsi que des mises à jour de sécurité régulières pour toute
    la durée de vie des paquets.
  </dd>
</dl>

<dl>
  <dt><strong>Debian dispose d'une vaste prise en charge matérielle.</strong></dt>
  <dd>
    L'essentiel des matériels est déjà pris en charge par le noyau Linux ce qui
    signifie que Debian les gérera aussi. Des pilotes propriétaires sont 
    disponibles si nécessaires.
  </dd>
</dl>

<dl>
  <dt><strong>Debian dispose d'un installateur souple.</strong></dt>
  <dd>
    Le <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">CD
    autonome (« live »)</a> est fait pour tous ceux qui veulent essayer Debian
    avant de l'installer. Il fournit également l'installateur Calamares qui facilite
    l'installation de Debian à partir du système autonome. Les utilisateurs plus
    expérimentés peuvent utiliser l'installateur Debian offrant plus d'options
    pour peaufiner leur installation, y compris la possibilité d'utiliser un
    outil d'installation automatisée par le réseau.
  </dd>
</dl>

<dl>
  <dt><strong>Debian offre des mises à niveau en douceur.</strong></dt>
  <dd>
    Il est facile de garder son système d'exploitation à jour, que ce soit pour
    mettre à niveau entièrement vers une nouvelle publication ou juste mettre à
    jour un paquet unique.
  </dd>
</dl>

<dl>
  <dt><strong>Debian est la base de nombreuses autres distributions.</strong></dt>
  <dd>
    Beaucoup des distributions Linux les plus populaires, telles qu'Ubuntu,
    Knoppix, PureOS ou Tails, ont choisi Debian comme base. Debian fournit tous
    les outils de telle sorte que tout le monde puisse accroître ses paquets
    logiciels en partant de l'archive Debian avec ses propres paquets pour
    répondre à ses besoins.
  </dd>
</dl>

<dl>
  <dt><strong>Le projet Debian est une communauté.</strong></dt>
  <dd>
    Tout le monde peut faire partie de la communauté Debian ; il n'est pas
    besoin d'être un programmeur ou un administrateur système. Debian est dotée
    d'une <a href="../devel/constitution">structure de gouvernance démocratique</a>.
    Dans la mesure où tous les développeurs Debian ont des droits égaux, elle ne
    peut pas être contrôlée par une entreprise unique. Nous avons des
    développeurs dans plus de 60 pays et Debian elle-même est traduite en plus
    de 80 langues.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> La raison qui sous-tend la reconnaissance de Debian comme système d'exploitation pour développeur est le grand nombre de paquets et de prises en charge de logiciels, ce qui est important pour les développeurs. Elle est fortement recommandée pour les programmeurs avancés et les administrateurs systèmes. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma dans Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian pour les développeurs</a></h2>

<dl>
  <dt><strong>De multiples architectures matérielles.</strong></dt>
  <dd>
    Debian prend en charge une <a href="../ports">longue liste</a>
    d'architectures de processeur dont amd64, i386, plusieurs versions d'ARM
    ainsi que MIPS, POWER7, POWER8, IBM System z et RISC-V. Debian est aussi
    disponible pour des architectures de niche anciennes ou spécifiques.
  </dd>
</dl>

<dl>
  <dt><strong>Internet des objets et périphériques embarqués.</strong></dt>
  <dd>
    Debian prend en charge une large gamme d'appareils tels que le Raspberry
    Pi, des variantes de QNAP, des appareils mobiles, des routeurs domestiques
    et beaucoup d'ordinateurs monocartes (Single Board Computers – SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Un nombre immense des paquets logiciels disponibles.</strong></dt>
  <dd>
    Debian possède un grand nombre de paquets (actuellement <packages_in_stable>
    paquets dans stable). Les paquets utilisent le <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">format
    deb</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Un choix de différentes versions.</strong></dt>
  <dd>
    En plus de la version stable, on peut obtenir les versions les plus
    récentes des logiciels en utilisant les versions <em>testing</em> ou
    <em>unstable</em>.
  </dd>
</dl>

<dl>
  <dt><strong>Un système de suivi des bogues accessible au public.</strong></dt>
  <dd>
    Le <a href="../Bugs">système de suivi des bogues</a> (Bug tracking system
    – BTS) de Debian est accessible à tous au moyen d'un navigateur web. Nous 
    ne cachons pas les bogues de nos logiciels et il est facile de soumettre de
    nouveaux rapports de bogue ou de participer à la discussion.
  </dd>
</dl>

<dl>
  <dt><strong>Des outils de développement et la charte Debian.</strong></dt>
  <dd>
    Debian offre des logiciels de haute qualité. Pour en savoir plus sur ses
    normes, consultez la <a href="../doc/debian-policy/">charte</a> qui définit
    les exigences techniques que chaque paquet doit satisfaire afin d'être
    inclus dans la distribution. L'intégration continue se sert du logiciel
    Autopkgtest (réalisation de tests sur les paquets), Piuparts (tests
    d'installation, de mise à niveau et de suppression de paquet) et Lintian (un
    vérificateur des incohérences et erreurs des paquets).
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Debian est synonyme de stabilité. [...]  La sécurité est une des caractéristiques majeures de Debian. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis dans pontikis.net</a></p>

</aside>
<h2><a id="enterprise">Debian dans un environnement d'entreprise</a></h2>

<dl>
  <dt><strong>Debian est fiable.</strong></dt>
  <dd>
    Debian démontre sa fiabilité tous les jours dans des milliers de scénarios
    du monde réel qui vont de l'ordinateur portable à utilisateur unique aux
    grands collisionneurs, bourses des valeurs ou industrie automobile. Elle est
    aussi répandue dans le monde universitaire, dans la recherche et dans le
    secteur public.
  </dd>
</dl>

<dl>
  <dt><strong>Debian compte de nombreux experts.</strong></dt>
  <dd>
    Les responsables de paquet ne font pas que s'occuper de l'empaquetage
    Debian et de l'incorporation des nouvelles versions amont. Souvent, ce
    sont des experts des logiciels eux-mêmes et donc ils contribuent directement
    au développement amont.
  </dd>
</dl>

<dl>
  <dt><strong>Debian est sûre.</strong></dt>
  <dd>
    Debian prend en charge la sécurité de ses versions stables. Beaucoup
    d'autres distributions et de chercheurs dans le domaine de la sécurité
    s'appuient sur le suivi de sécurité de Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Prise en charge à long terme.</strong></dt>
  <dd>
    Il existe une <a href="https://wiki.debian.org/LTS">prise en charge à long terme</a>
    (Long Term Support – LTS) gratuite qui apporte une prise en charge
    étendue pour la version stable pour au moins 5 ans. Au-delà, l'initiative
    commerciale
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a> étend la 
    prise en charge d'un ensemble limité de paquets à plus de 5 ans.
  </dd>
</dl>

<dl>
  <dt><strong>Images « cloud ».</strong></dt>
  <dd>
    Des images officielles pour le nuage sont disponibles pour toutes les
    principales plateformes de nuage. Nous fournissons aussi les outils et les
    configurations pour que vous puissiez construire votre propre image
    personnalisée pour le nuage. Il est aussi possible d'utiliser Debian dans
    des machines virtuelles sur le bureau ou dans un conteneur.
  </dd>
</dl>
