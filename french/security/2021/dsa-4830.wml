#use wml::debian::translation-check translation="cc173b8d34b89c7d43e8628759e88ae4a67b7db9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Simon McVittie a découvert un bogue dans le service flatpak-portal qui
peut permettre à des applications confinées dans un bac à sable d'exécuter
du code arbitraire dans le système hôte (sortie du bac à sable).</p>

<p>Le service D-Bus de portail de Flatpak (flatpak-portal, aussi connu sous
son nom de service D-Bus org.freedesktop.portal.Flatpak) permet à des
applications dans un bac à sable Flatpak de lancer leurs propres
sous-processus dans une nouvelle instance de bac à sable, soit avec les
même paramètres de sécurité que l'appelant, soit avec des réglages de
sécurités plus contraignants. Par exemple, cela est utilisé par les
navigateurs web empaquetés par Flatpak tels que Chromium pour lancer des
sous-processus qui vont traiter du contenu web non fiable et donner à ces
sous-processus un confinement plus restrictif que celui du navigateur
lui-même.</p>

<p>Dans les versions vulnérables, le service de portail de Flatpak passe
des variables d'environnement spécifiques à l'appelant à des processus non
confinés sur le système hôte, et en particulier à la commande d'exécution
de flatpak qui est utilisée pour lancer une nouvelle instance de bac à sable.
Une application Flatpak malveillante ou compromise pourrait affecter des
variables d'environnement considérées fiables par la commande d'exécution
de flatpak et les utiliser pour exécuter du code arbitraire qui n'est pas
dans un bac à sable.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1.2.5-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets flatpak.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de flatpak, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4830.data"
# $Id: $
