#use wml::debian::template title="Errata pour l'installateur Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="50ddc8fab8f8142c1e8266a7c0c741f9bfe1b23a" maintainer="Baptiste Jammet" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

# Translators:
# Nicolas Bertolissio, 2003-2008
# Thomas Peteul, 2008, 2009
# Simon Paillard, 2010
# David Prévot, 2010-2014
# Jean-Pierre Giraud, 2013, 2021
# Baptiste Jammet, 2014-2019

<h1><i>Errata</i> de la version <humanversion /></h1>

<p>
Voici une liste des problèmes connus dans la version <humanversion />
de l'installateur Debian.
Si le problème rencontré n'y est pas recensé, veuillez envoyer un <a
href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">compte-rendu d'installation</a>
(en anglais) décrivant le problème.
</p>

<dl class="gloss">
<dt>Mode de récupération cassé avec l’installateur graphique</dt>
<dd>Il a été constaté lors du test de l’image RC 1 de Bullseye que le mode
de récupération était cassé (<a href="https://bugs.debian.org/987377">n° 987377</a>).
De plus, l’étiquette <q>Rescue</q> dans la bannière nécessite d’être ajustée
pour le thème de Bullseye.
<br />
<b>État :</b> Corrigé dans Bullseye RC 2.</dd>

<dt>Micrologiciel amdgpu nécessaire pour de nombreuses cartes graphiques AMD</dt>
<dd>Il apparaît que le besoin d’installer le micrologiciel amdgpu soit croissant
(à l’aide du paquet non libre <code>firmware-amd-graphics</code>) pour éviter
un écran noir lors de l’amorçage du système installé. Depuis la RC 1 de Bullseye,
même en utilisant une image d’installation incluant tous les paquets de
micrologiciels, l’installateur ne détecte pas le besoin de ce composant
spécifique. Voir le <a href="https://bugs.debian.org/989863">rapport de bogue
générique</a> pour suivre la progression de notre travail.
<br />
<b>État :</b> Corrigé dans Bullseye RC 3.</dd>

<dt>Micrologiciels nécessaires pour certaines cartes son</dt>
<dd>Il apparaît qu'il y a un certain nombre de cartes son qui nécessitent le
chargement d'un micrologiciel pour pouvoir émettre un son. Depuis Bullseye,
l'installateur n'est pas capable de le faire tôt, ce qui signifie que la
synthèse vocale n'est pas possible pendant l'installation avec ces cartes. Une
solution possible est de brancher une autre carte son qui n'a pas besoin d'un
micrologiciel. Voir le <a href="https://bugs.debian.org/992699">rapport de bogue
générique</a> pour suivre la progression de notre travail.</dd>

<dt>L'installation d'un environnement de bureau pourrait échouer avec le seul premier CD</dt>
<dd>
En raison de sa taille,
la totalité des paquets du bureau GNOME ne rentre pas sur le premier CD.
Pour réussir cette installation, utilisez une source supplémentaire de paquets
(un second CD ou un miroir réseau) ou utilisez plutôt un DVD.
<br />
<b>État :</b> rien de plus ne peut être fait pour faire tenir
davantage de paquets sur le premier CD.
</dd>

<dt>LUKS2 n'est pas compatible avec cryptodisk de GRUB</dt>
<dd>Il a été découvert tardivement que GRUB ne prenait pas en charge LUKS2.
Cela veut dire qu'il n'est pas possible d'utiliser <tt>GRUB_ENABLE_CRYPTODISK</tt>
sans une partition <tt>/boot</tt> séparée et non chiffrée.
(<a href="https://bugs.debian.org/927165">n° 927165</a>).
L'installateur ne prend pas en charge cette configuration,
mais il serait intéressant de mieux documenter cette limitation,
et d'avoir au moins la possibilité de choisir LUKS1 pendant l'installation.
<br />
<b>État :</b> quelques idées ont été exprimées dans le rapport de bogue.
Les responsables de cryptsetup ont écrit
<a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">une
documentation spécifique</a> (en anglais).</dd>

<!-- Les choses devraient être mieux à partir de Jessie Beta 2...

Les porteurs ne refuseraient sans doute pas un peu
d’aide pour remettre l’installateur en service.
</dd>
-->
<!--
<dt>Accessibilité sur le système installé</dt>
<dd>Même si les technologies d'accessibilité sont utilisées pendant l'installation,
elles pourraient ne pas être activées automatiquement dans le système installé.
</dd>
-->

<!-- conservé pour une possible utilisation ultérieure...
	<dt>Installations de bureau non fonctionnelles sur i386 à partir du premier CD seul</dt>
	<dd>
	À cause de contraintes de place, tous les paquets nécessaires
	au bureau GNOME ne peuvent pas tenir sur le premier CD.

	Pour réussir l'installation, utilisez des sources de
	paquets supplémentaires (par exemple un deuxième CD ou
	un miroir sur le réseau) ou utilisez plutôt un DVD.
	<br />
	<b>État :</b> aucune ressource ne pourra probablement être consacrée
	à l'inclusion de paquets supplémentaires sur le premier CD.
	</dd>
-->

<!-- idem
	<dt>Problèmes possibles avec l'amorçage UEFI sur amd64</dt>
	<dd>
	Des problèmes ont été signalés lors de l'amorçage de
	l'installateur Debian en mode UEFI sur des systèmes amd64.

	Certains systèmes ne démarrent apparemment pas de façon fiable avec
	<code>grub-efi</code> et d'autres montrent des problèmes de
	corruption graphique de l'affichage d'écran initial d'installation.
	<br />
	Si vous rencontrez un de ces deux problèmes, veuillez soumettre un
	rapport de bogue aussi précis que possible, à la fois sur les symptômes
	et sur le matériel — cela devrait aider l'équipe à corriger ces problèmes.

	En attendant, il est possible de contourner le problème en désactivant l'UEFI et en
	poursuivant l'installation avec l'<q>ancien BIOS</q> ou en <q>mode de secours</q>.
	<br />
	<b>État :</b> d'autres corrections de bogues pourraient
	apparaître dans les diverses mises à jour de Wheezy.
	</dd>
-->
</dl>
