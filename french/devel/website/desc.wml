#use wml::debian::template title="Comment est généré www.debian.org"
#use wml::debian::translation-check translation="7f876037d8c7ab141d9c245fcd09a3b77e25b3a7" maintainer="Jean-Pierre Giraud"
#use wml::debian::toc

# Translators:
# Denis Barbier, 2001-2004.
# Pierre Machard, 2002.
# Frederic Bothamy, 2003, 2007.
# David Prévot, 2010, 2011.
# Jean-Pierre Giraud, 2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">«&nbsp;Look &amp; feel&nbsp;»</a></li>
<li><a href="#sources">Sources</a></li>
<li><a href="#scripts">Scripts</a></li>
<li><a href="#generate">Génération du site Web</a></li>
<li><a href="#help">Comment aider</a></li>
<li><a href="#faq">Comment <strong>ne pas</strong> aider… (FAQ))</a></li>
</ul>

<h2><a id="look">«&nbsp;<em>Look &amp; feel</em>&nbsp;»</a></h2>

<p>Le site web de Debian est un ensemble de répertoires et de fichiers qui se
trouve dans le répertoire <code>/org/www.debian.org/www</code> sur la machine
<em>www-master.debian.org</em>. Les pages sont essentiellement des pages
HTML statiques. C'est-à-dire qu'elles ne contiennent pas d'éléments dynamiques
tels que des scripts CGI ou PHP, parce que le site est recopié par des sites
miroirs.
</p>

<p>
Le site Web de Debian utilise le «&nbsp;Website Meta Language&nbsp;»
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>) pour générer
les pages HTML, y compris les en-têtes et les pieds-de-page, les titres, les
tables des matières, etc. Bien qu'une page <code>.wml</code> apparaisse de
prime abord comme étant une page HTML, le HTML n'est qu'un des types
d'informations supplémentaires pouvant être contenus dans des fichiers WML.
Vous pouvez aussi inclure du code Perl dans une page pour vous permettre de
faire à peu près tout ce que vous souhaitez.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Actuellement, nos pages web sont conformes à la norme <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.</p>
</aside>

<p>
Une fois que WML a terminé ses divers filtrages sur un fichier, le résultat
final est du HTML. Il faut cependant noter que bien que WML vérifie (et parfois
corrige de façon automatique) la validité du code HTML de façon basique, vous
devriez installer un outil tel que
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
et/ou
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
pour valider la syntaxe et un minimum de style.
</p>

<p>Quiconque contribue régulièrement au site web de Debian devrait installer
WML afin de tester le code et de vérifier que les pages HTML obtenues ont une
apparence satisfaisante. Si vous utilisez Debian, vous pouvez simplement
installer le paquet <code>wml</code>. Consultez la page
<a href="using_wml">comment utiliser WML</a> pour de plus amples informations.
</p>

<h2><a id="sources">Sources</a></h2>

<p>Les sources des pages du site web de Debian sont gérées par Git. Ce système
de contrôle de version qui permet de voir qui, quoi, quand et même pourquoi a
modifié certains fichiers. Git permet un accès aux sources simultané par
plusieurs auteurs de façon sûre, ce qui est crucial pour nous, parce que
l'équipe du site web de Debian est nombreuse.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Comment utiliser Git</a></button></p>

<p>
Voici quelques informations de base sur comment sont structurées les sources :
</p>

<ul>
  <li>Le répertoire racine du dépôt Git (<code>webwml</code>) contient des
répertoires dont le nom est celui des langues dans lesquelles ce site
est traduit, deux fichiers Makefile et plusieurs scripts. Le nom du
répertoire de traduction doit être en anglais et en lettres minuscules,
par exemple «&nbsp;french&nbsp;» et non «&nbsp;Français&nbsp;».</li>

  <li>Le fichier <code>Makefile.common</code> particulièrement important parce
qu'il contient quelques règles communes qui sont appliquées en incluant ce
fichier dans les autres Makefile.</li>

  <li>Chacun des répertoires de traduction contient aussi un Makefile, divers
fichiers source <code>.wml</code> et des sous-répertoires. Les noms des
fichiers et des répertoires suivent un certain modèle afin que tous les liens
fonctionnent dans toutes les langues. Les répertoires peuvent aussi avoir
des fichiers de configuration <code>.wmlrc</code> qui contiennent des commandes
et des préférences pour WML.</li>

  <li>Le répertoire <code>webwml/english/template</code> contient des fichiers
WML spéciaux qui fonctionnent comme modèles («&nbsp;templates&nbsp;» en
anglais). Ils peuvent être appelés depuis n'importe quel autre fichier en
utilisant la commande <code>#use</code>.</li>
</ul>

<p>
Remarque : afin que les changements sur ces fichiers modèles se répercutent aux
fichiers qui les utilisent, des dépendances ont été mises dans les Makefile.
Comme une grande majorité de fichiers utilisent le modèle
<code>template</code>, ils contiennent une ligne en haut de la page :
</p>

<p>
<code>#use wml::debian::template</code>
</p>

<p>
Bien sûr, il y a des exceptions à cette règle.
</p>

<h2><a id="scripts">Scripts</a></h2>

<p>
Les scripts sont majoritairement écrits en shell ou en Perl. Quelques-uns
sont indépendants, d'autres sont intégrés dans les fichiers sources WML.
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a> :
Ce dépôt Git contient tous les scripts utilisés pour mettre à jour le site web,
c'est-à-dire les sources des scripts de reconstruction du site <code>www-master</code>.</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a> : 
Ce dépôt Git contient les sources des scripts pour reconstruire <code>packages.debian.org</code>.</li>
</ul>

<h2><a id="generate">Génération du site web</a></h2>

<p>
Fichiers WML, templates et scripts shell ou en Perl sont tous les ingrédients
nécessaires à la génération du site web de Debian :
</p>

<ul>
  <li>l'essentiel est généré avec WML (à partir du <a href="$(DEVEL)/website/using_git">dépôt Git</a>) ;</li>
  <li>la documentation est générée soit par XML DocBook (<a href="$(DOC)/vcs">dépôt Git <q>ddp</q></a>)
soit en utilisant les <a href="#scripts">scripts cron</a> des paquets Debian
correspondants ;</li>
  <li>certaines parties du site sont générées à partir de scripts utilisant
d'autres sources, par exemple, la page d'inscription aux listes de diffusion.</li>
</ul>

<p>
Une mise à jour automatique (à partir du dépôt Git et d'autres sources de
l’arborescence) est effectuée six fois par jour. À part cela, les vérifications
suivantes sont régulièrement lancées sur la totalité du site :
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">vérification des liens</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
Les journaux de la dernière construction du site web sont accessibles sur
<url "https://www-master.debian.org/build-logs/">.
</p>

<p>Si vous souhaitez contribuer au site, <strong>il ne faut pas</strong>
simplement modifier les fichiers dans le répertoire <code>www/</code> ou
ajouter de nouveaux sujets. Prenez plutôt d'abord contact avec
<a href="mailto:webmaster@debian.org">les webmasters</a>.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span> D'un point de vue technique :
<p>Tous les répertoires et fichiers appartiennent au groupe <code>debwww</code>
qui bénéficie du droit d'écriture. Ainsi, l'équipe du site web peut modifier
le contenu du répertoire web. Le mode <code>2775</code> sur les répertoires
signifie que tous les fichiers créés dans cette arborescence appartiendront
aussi à ce groupe (<code>debwww</code>). Les membres du groupe doivent donc
faire «&nbsp;<code>umask 002</code>&nbsp;» afin que les fichiers créés soient
accessibles en écriture pour le groupe.</p>
</aside>

<h2><a id="help">Comment aider</a></h2>

<p>
Nous invitons quiconque à nous aider pour améliorer le site web de Debian.
Si vous avez des informations intéressantes sur Debian qui manquent sur le site,
<a href="mailto:debian-www@lists.debian.org">prenez contact avec nous</a>
et nous veillerons à ce qu'elles soient incluses. Veuillez aussi consulter les
<a href="https://www-master.debian.org/build-logs/">journaux de construction</a>
mentionnés plus haut et voyez si vous avez des suggestions pour corriger un
problème.
</p>

<p>
Nous avons toujours besoin d'aide pour améliorer le design des pages
(d'un point de vue graphique et présentation). Si vous parlez couramment
l'anglais, vous pouvez vérifier les pages et
<a href="mailto:debian-www@lists.debian.org">nous indiquer</a>
toute erreur. Si vous parlez une autre langue, vous pouvez aussi nous aider
à traduire les pages ou aider à corriger des erreurs dans les pages déjà
traduites. Dans les deux cas, regardez la liste des
<a href="translation_coordinators">coordinateurs de traduction</a> et indiquez
ces problèmes au responsable de la traduction. Pour de plus amples informations,
consultez <a href="translating">la page pour les traducteurs</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Regardez notre « TODO list »</a></button></p>

<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">Comment <strong>ne pas</strong> aider... (FAQ)</a></h2>

<p>
<strong>[Q] Je veux ajouter cette <em>fonctionnalité amusante</em> au site web de Debian, je peux ?</strong>
</p>

<p>
Non. Nous voulons que www.debian.org soit le plus accessible possible, ce qui
implique
</p>

<ul>
    <li>pas d'extensions propriétaires des navigateurs ;
    <li>pas de navigation utilisant exclusivement des images. Les images
        peuvent être utilisées pour illustrer, mais l'information sur
        www.debian.org doit être accessible avec un navigateur en mode
        texte, comme lynx.
</ul>

<p>
<strong>[Q] J'ai une idée sympa à soumettre. Pouvez-vous ajouter <em>truc</em>
ou <em>bidule</em> dans le serveur HTTP de www.debian.org ?</strong>
</p>

<p>
[R] Non. Nous voulons que les administrateurs des sites miroirs de
www.debian.org aient une tâche facile, donc aucune fonctionnalité exotique de
HTTPD, s'il vous plaît. Non, même pas les <em>Server Side Includes</em> (SSI).
Une exception a été faite pour la négociation du contenu, parce que c'est la
seule façon fiable de permettre une navigation multilingue.
</p>
