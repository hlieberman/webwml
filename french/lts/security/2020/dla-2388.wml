#use wml::debian::translation-check translation="1125eb8325161894b3d91a609f256d57631a549a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Diverses vulnérabilités ont été corrigées dans nss, les bibliothèques du
Network Security Service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12404">CVE-2018-12404</a>

<p>Variante de l’attaque Bleichenbacher par canal auxiliaire de cache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18508">CVE-2018-18508</a>

<p>Déréférencement de pointeur NULL dans plusieurs fonctions CMS aboutissant
à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">CVE-2019-11719</a>

<p>Lecture hors limites lors de l’importation de clés curve25519 privées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">CVE-2019-11729</a>

<p>Clés publiques vides ou mal formées p256-ECDH pouvant déclencher une erreur
de segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11745">CVE-2019-11745</a>

<p>Écriture hors limites lors d’un chiffrement par bloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17006">CVE-2019-17006</a>

<p>Quelques primitives de chiffrement ne vérifiaient pas la longueur du texte
entré, aboutissant éventuellement à des débordements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17007">CVE-2019-17007</a>

<p>Gestion des Netscape Certificate Sequence pouvant planter avec un déférencement
de pointeur NULL conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12399">CVE-2020-12399</a>

<p>Forçage d’une longueur fixe pour l’exponentiation DSA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6829">CVE-2020-6829</a>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12400">CVE-2020-12400</a>

<p>Attaque par canal auxiliaire sur la génération de signature ECDSA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12401">CVE-2020-12401</a>

<p>Contournement de la mitigation d’attaque temporelle sur ECDSA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12402">CVE-2020-12402</a>

<p>Vulnérabilités de canal auxiliaire lors de la génération de clé RSA.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12403">CVE-2020-12403</a>

<p>Déchiffrement CHACHA20-POLY1305 avec des mots clés sous-dimensionnés
aboutissant à une lecture hors limites.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:3.26.2-1.1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nss, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nss">https://security-tracker.debian.org/tracker/nss</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2388.data"
# $Id: $
