#use wml::debian::translation-check translation="91e9e0da7a452359c7acb051ea8260a29b255826" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Wordpress, un outil de
blog. Elles permettaient à des attaquants distants de réaliser diverses attaques
de script intersite (XSS), de créer des redirections ouvertes, d'élever leurs
privilèges et de contourner les autorisations d'accès.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4046">CVE-2020-4046</a>

<p>Dans les versions touchées de WordPress, les utilisateurs avec peu de
privilèges (comme les contributeurs et les rédacteurs) peuvent utiliser les
blocs embarqués d’une certaine façon pour injecter du HTML non filtré dans
l’éditeur de bloc. Lorsque les billets affectés étaient lus par un utilisateur
de plus hauts privilèges, cela pouvait conduire à une exécution de script dans
editor/wp-admin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4047">CVE-2020-4047</a>

<p>Dans les versions touchées de WordPress, des utilisateurs authentifiés avec
permission de téléversement (comme les rédacteurs) étaient capables d’injecter
du JavaScript dans des pages de fichier de média joint d’une certaine façon.
Cela pouvait conduire à une exécution de script dans le contexte d’un
utilisateur plus privilégié quand il lisait un fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4048">CVE-2020-4048</a>

<p>Dans les versions touchées de WordPress, dû à un problème dans
wp_validate_redirect() et un nettoyage d’URL, un lien arbitraire externe pouvait
être contrefait conduisant à une redirection inattendue ou ouverte lors d’un
clic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4049">CVE-2020-4049</a>

<p>Dans les versions touchées de WordPress, lors du téléversement de thème, le
nom du dossier du thème pouvait être contrefait d’une certaine façon pouvant
conduire à une exécution de JavaScript dans /wp-admin dans la page de thèmes.
Cela exigeait qu’un administrateur téléverse le thème, et est une attaque
self-XSS de faible niveau de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4050">CVE-2020-4050</a>

<p>Dans les versions touchées de WordPress, une mauvaise utilisation de la
valeur de retour « set-screen-option » de filtre permettait à des champs meta
d’utilisateur arbitraire d’être enregistrés. Cela nécessitait qu’un
administrateur installe un greffon utilisant mal le filtre. Une fois installé,
il pouvait être exploité par des utilisateurs peu privilégiés.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.1.31+dfsg-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2269.data"
# $Id: $
