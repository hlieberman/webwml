#use wml::debian::translation-check translation="337268b83bf8bed7da71224da2cd4bb9052bf8c6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Perl5 Database Interface
(DBI). Un attaquant pourrait déclencher un déni de service (DoS) et
éventuellement exécuter du code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20919">CVE-2019-20919</a>

<p>La documentation de hv_fetch() demande une vérification pour NULL et le code
le réalise. Mais, peu de temps après, il appelle SvOK(profile), provoquant un
déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14392">CVE-2020-14392</a>

<p>Un défaut de déréférencement de pointeur non fiable a été découvert dans
Perl-DBI. Un attaquant local capable de manipuler des appels
à dbd_db_login6_sv() pourrait causer une corruption de mémoire, affectant la
disponibilité du service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14393">CVE-2020-14393</a>

<p>Un dépassement de tampon à l'aide d'un nom de classe DBD trop long dans la
fonction dbih_setup_handle pourrait conduire à l’écriture de données au-delà de
la limite voulue.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.636-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libdbi-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libdbi-perl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libdbi-perl">https://security-tracker.debian.org/tracker/libdbi-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2386.data"
# $Id: $
