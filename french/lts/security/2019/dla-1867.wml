#use wml::debian::translation-check translation="d2dec98b4e629c75452b2db15f38ef5a59f36e7c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans supplicant et hostapd de
WPA. Quelques-unes pourraient être atténuées partiellement, veuillez lire la
suite pour plus de détails.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

<p>Une attaque par canal auxiliaire basée sur le cache à l'encontre de
l'implémentation de EAP-pwd : un attaquant ayant la possibilité d'exécuter du
code sans droits sur la machine cible (y compris par exemple du code javascript
dans un navigateur sur un ordiphone) durant la négociation de connexion pourrait
déduire suffisamment d'informations pour découvrir le mot de passe dans une
attaque par dictionnaire.</p>

<p>Ce problème a été seulement très partiellement tempéré en réduisant les
différences mesurables d’horodatage lors d'opérations sur la clef privée. Plus de
travail est nécessaire pour supprimer complètement cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

<p>Une attaque par réflexion à l'encontre de l'implémentation du serveur
EAP-pwd : un manque de validation de la valeur des scalaires et éléments dans
les messages EAP-pwd-Commit pourrait avoir pour conséquence des attaques qui
permettraient de réussir des échanges d'authentification EAP-pwd sans que
l'attaquant ait à connaître le mot de passe. Cela n'aboutit pas à ce que
l'attaquant soit capable d'obtenir la clé de session, de réussir l'échange de
clé subséquent puis d'accéder au réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

<p>L'absence de validation par le serveur EAP-pwd de « commit » pour les
scalaires et les éléments : hostapd ne valide pas les valeurs reçues dans le
message EAP-pwd-Commit, aussi un attaquant pourrait utiliser un message de
« commit » contrefait pour l'occasion pour manipuler les échanges afin que
hostapd obtienne une clé de session à partir d'un ensemble limité de valeurs
possibles. Cela pourrait avoir pour conséquence qu'un attaquant soit capable de
réussir une authentification et obtienne l'accès au réseau.</p>

<p>Ce problème pourrait n’être que partiellement atténué.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

<p>L'absence de validation par le pair EAP-pwd de « commit » pour les scalaires
et les éléments : wpa_supplicant ne valide pas les valeurs reçues dans le
message EAP-pwd-Commit, aussi un attaquant pourrait utiliser un message de
« commit » contrefait pour l'occasion pour manipuler les échanges afin que
wpa_supplicant obtienne une clé de session à partir d'un ensemble limité de
valeurs possibles. Cela pourrait avoir pour conséquence qu'un attaquant soit
capable de réussir une authentification et fonctionne comme un protocole
d'authentification (AP) véreux.</p>

<p>Ce problème pourrait n’être que partiellement atténué.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11555">CVE-2019-11555</a>

<p>L’implémentation de EAP-pwd ne validait pas correctement l’état de
réassemblage de fragmentation lors de la réception d’un fragment inattendu. Cela
pourrait avoir conduit à un plantage du processus à cause d’un déréférencement
de pointeur NULL.</p>

<p>Un attaquant dans la zone de réception radio d’une station ou d’un point
d’accès avec la prise en charge EAP-pwd pourrait provoquer un plantage du
processus concerné (wpa_supplicant ou hostapd), garantissant un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.3-1+deb8u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1867.data"
# $Id: $
