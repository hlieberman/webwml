#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Cette mise à jour corrige deux vulnérabilités dans ImageMagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15277">CVE-2017-15277</a>

<p>Une structure de données non initialisée pourrait conduire à une divulgation
d'informations lors de la lecture d’un fichier GIF contrefait spécialement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15281">CVE-2017-15281</a>

<p>Une valeur non initialisée utilisée dans un saut conditionnel pourrait causer
un déni de service (plantage d'application) ou avoir d’autres impacts non
précisés lors de la lecture d’un fichier PSD contrefait spécialement.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 8:6.7.7.10-5+deb7u18.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1139.data"
# $Id: $
