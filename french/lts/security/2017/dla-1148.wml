#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Go avant 1.8.4 et 1.9.x avant 1.9.1 permettent une exécution de commande
<q>go get</q> à distance. En utilisant des domaines personnalisés, il est
possible de parvenir à ce que example.com/pkg1 pointe vers un dépôt Subversion
mais que example.com/pkg1/pkg2 pointe vers un dépôt Git. Si le dépôt
Subversion inclut un checkout de Git dans son répertoire pkg2 et que quelque
chose soit fait pour garantir un bon ordonnancement des opérations, <q>go
get</q> peut être piégé à réutiliser ce checkout de Git pour extraire du code de
pkg2. Si le checkout de Subversion pour le dépôt de Git possède des commandes
malveillantes dans .git/hooks/, elles s’exécuteront sur le système utilisant
« go get ».</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2:1.0.2-1.1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1148.data"
# $Id: $
