#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6298">CVE-2017-6298</a>

<p>Déréférencement de pointeur Null et valeur de renvoi de calloc non vérifiée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6299">CVE-2017-6299</a>

<p>Boucle infinie et déni de service dans la fonction TNEFFillMapi dans lib/ytnef.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6300">CVE-2017-6300</a>

<p>Dépassement de tampon dans le champ de version dans lib/tnef-types.h.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6301">CVE-2017-6301</a>

<p>Lectures hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6302">CVE-2017-6302</a>

<p>Dépassement d'entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6303">CVE-2017-6303</a>

<p>Écriture non valable et dépassement d'entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6304">CVE-2017-6304</a>

<p>Lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6305">CVE-2017-6305</a>

<p>Lecture et écritures hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6801">CVE-2017-6801</a>

<p>Accès hors limites avec des champs de taille nulle dans TNEFParse() dans libytnef.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6802">CVE-2017-6802</a>

<p>Lecture hors limites de tampon de tas pour des flux RTF entrants et
compressés, relatifs à DecompressRTF() dans libytnef.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.5-4+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libytnef.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-878.data"
# $Id: $
