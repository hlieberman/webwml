#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans l’interpréteur pour le langage Ruby.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>

<p>Aaron Patterson a signalé que WEBrick empaqueté avec Ruby était vulnérable
à une vulnérabilité de fractionnement de réponse HTTP. Il était possible pour un
attaquant d’injecter des réponses fausses si un script acceptait une entrée
externe et la sortait sans modification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6914">CVE-2018-6914</a>

<p>ooooooo_q a découvert une vulnérabilité de traversée de répertoires dans la
méthode Dir.mktmpdir de la bibliothèque tmpdir. Elle permettait à des
attaquants de créer des répertoires ou fichiers arbitraires l'aide d'un « .. »
(point point) dans l’argument du préfixe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8777">CVE-2018-8777</a>

<p>Eric Wong a signalé une vulnérabilité de déni de service par épuisement de
mémoire, relative à une large requête dans WEBrick empaqueté avec Ruby.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8778">CVE-2018-8778</a>

<p>aerodudrizzt a trouvé une vulnérabilité de lecture hors limites dans la
méthode String#unpack de Ruby. Si un grand nombre était passé avec le
spécificateur @, le nombre était traité comme une valeur négative et une lecture
hors limite se produisait. Des attaquants pourraient lire des données de tas
si un script acceptait une entrée externe comme argument de String#unpack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8779">CVE-2018-8779</a>

<p>ooooooo_q a signalé que les méthodes UNIXServer.open et UNIXSocket.open de la
bibliothèque de socket empaquetée avec Ruby ne vérifiait pas si des octets étaient NUL
dans l’argument du chemin. Le manque de vérification rend les méthodes
vulnérables à la création et à l’accès involontaire de socket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8780">CVE-2018-8780</a>

<p>ooooooo_q a découvert une traversée de répertoires involontaire dans quelques
méthodes dans Dir, à cause du manque de vérification des octets NUL dans
leurs paramètres.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.8.7.358-7.1+deb7u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ruby1.8.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1359.data"
# $Id: $
