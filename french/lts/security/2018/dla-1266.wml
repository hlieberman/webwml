#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert que Squid, un serveur mandataire cache à haute
performance pour des clients web, était vulnérable à des attaques par déni
de service associées au traitement de réponses ESI et au téléchargement de
certificats d'autorités de certification (CA) intermédiaires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000024">CVE-2018-1000024</a>

<p>Un traitement incorrect de pointeur avait pour conséquence de permettre
à un serveur distant de renvoyer une réponse ESI contrefaite qui pourrait
déclencher un déni de service pour tous les clients accédant au service
Squid. Ce problème affecte l'analyseur ESI personnalisé de Squid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000027">CVE-2018-1000027</a>

<p>Un traitement incorrect de pointeur avait pour conséquence la
possibilité pour un client distant transmettant certaines requêtes HTTP, en
conjonction avec certaines réponses de serveur de confiance impliquant le
traitement de réponses ESI ou le téléchargement de certificats d'autorités
de certification (CA) intermédiaires, de déclencher un déni de service
pour tous les clients accédant au service Squid.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.1.20-2.2+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1266.data"
# $Id: $
