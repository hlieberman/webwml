#use wml::debian::translation-check translation="5d066b2fcb4127e3277af6b2d006b23f9f8ed133" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Marcel Neumann, Robert Altschaffel, Loris Guba et Dustin Hermann
ont découvert que debian-edu-config, un ensemble de fichiers de
configuration utilisé pour le mélange Debian Edu, configurait des
permissions non sécurisées pour les partages web de l'utilisateur
(~/public_html) ce qui pouvait avoir pour conséquence une élévation de
privilèges.</p>

<p>Si une fonctionnalité de PHP est nécessaire pour les partages web de
l'utilisateur, consultez la page
/usr/share/doc/debian-edu-config/README.public_html_with_PHP-CGI+suExec.md</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.929+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets debian-edu-config.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de debian-edu-config,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/debian-edu-config">\
https://security-tracker.debian.org/tracker/debian-edu-config</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2918.data"
# $Id: $
