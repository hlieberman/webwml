#use wml::debian::translation-check translation="3d55eb6e45c8822201f160827289792db115d551" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>nss, la bibliothèque de service de sécurité réseau de Mozilla, était
vulnérable à un déréférencement de pointeur NULL lors de l'analyse de
séquences PKCS 7 vides, ce qui pouvait avoir pour conséquence un déni de
service.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2:3.26.2-1.1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nss, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nss">\
https://security-tracker.debian.org/tracker/nss</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2898.data"
# $Id: $
