#use wml::debian::translation-check translation="d448f7a1713eb75ebf42f0258c392a5cc17c6490" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla, qui pourraient éventuellement avoir pour
conséquences l'exécution de code arbitraire, la divulgation d'informations
ou une usurpation.</p>

<p>Debian suit les éditions longue durée (ESR, « Extended Support
Release ») de Firefox. Le suivi des séries 78.x est terminé, aussi, à
partir de cette mise à jour, Debian suit les versions 91.x.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 91.4.1esr-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2863.data"
# $Id: $

