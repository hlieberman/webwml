#use wml::debian::translation-check translation="437a2c90c4cf4f8d46cbf9844dec4c82f084f44a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans gthumb, un visualisateur et gestionnaire
de photos. Un dépassement de tampon de tas dans
_cairo_image_surface_create_from_jpeg() dans
extensions/cairo_io/cairo-image-surface-jpeg.c permet à des attaquants de
provoquer un plantage et, éventuellement, d’exécuter du code arbitraire à
l'aide d'un fichier JPEG contrefait.</p>


<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 3:3.4.4.1-5+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gthumb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gthumb,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gthumb">\
https://security-tracker.debian.org/tracker/gthumb</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2749.data"
# $Id: $
