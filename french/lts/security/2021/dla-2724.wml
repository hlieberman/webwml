#use wml::debian::translation-check translation="0bb2782f9986a0ed173b7d6797eb0050fcbe9487" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>HTCondor, un système de gestion de charge distribuée incorporait un contrôle
d’accès incorrect. Il était possible d’utiliser une méthode d’authentification
pour soumettre une tâche différente de celle précisée par l’administrateur. Si
celui-ci avait configuré les méthodes READ ou WRITE pour inclure CLAIMTOBE,
alors il était possible d’usurper l’identité d’un autre utilisateur pour
condor_schedd, par exemple, pour soumettre ou retirer des travaux.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 8.4.11~dfsg.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets condor.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de condor,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/condor">\
https://security-tracker.debian.org/tracker/condor</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2724.data"
# $Id: $
