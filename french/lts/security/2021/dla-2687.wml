#use wml::debian::translation-check translation="57e4f2bd8a412531965a9b521b5dea0fbe631b36" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans prosody.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32917">CVE-2021-32917</a>

<p>Le composant proxy65 autorise par défaut un accès ouvert, même si aucun
utilisateur n’a un compte XMPP sur le serveur local, permettant une utilisation
non limitée de la bande passante du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32921">CVE-2021-32921</a>

<p>Le module d’authentification n’utilise pas un algorithme à temps constant
pour comparer certaines chaînes secrètes lors d’une exécution sous Lua 5.2 ou
une version ultérieure. Cela peut être utilisé éventuellement dans une attaque
temporelle pour révéler le contenu des chaînes secrètes.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.9.12-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets prosody.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de prosody, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/prosody">\
https://security-tracker.debian.org/tracker/prosody</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2687.data"
# $Id: $
