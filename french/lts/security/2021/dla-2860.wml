#use wml::debian::translation-check translation="9fd0c6282636c388d054098146413fc2b883e9f0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans paramiko, une
implémentation du protocole SSHv2 dans Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000805">CVE-2018-1000805</a>

<p>Correctif pour éviter que des clients malveillants amènent le serveur
Paramiko à penser qu'un client non authentifié est authentifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7750">CVE-2018-7750</a>

<p>Correctif de la vérification pour savoir si l'authentification est
achevée avant de traiter d'autres requêtes. Un client SSH personnalisé peut
sauter simplement l'étape d'authentification.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version

<p>Nous vous recommandons de mettre à jour vos paquets paramiko.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de paramiko, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/paramiko">\
https://security-tracker.debian.org/tracker/paramiko</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2860.data"
# $Id: $
