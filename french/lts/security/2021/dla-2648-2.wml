#use wml::debian::translation-check translation="6ab43a002a72dd6247c244201a8ba8ff33933bc3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le rectificatif de la dernière publication de l’amont, pour corriger le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-30152">CVE-2021-30152</a>,
n’était pas portable pour la version de sécurité de Stretch faisant que les API
de MediaWiki échouaient. Cette mise à jour inclut un correctif de la publication
REL_31 de l’amont qui règle ce problème.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1:1.27.7-1~deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2648-2.data"
# $Id: $
