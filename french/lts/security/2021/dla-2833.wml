#use wml::debian::translation-check translation="e5ff1e00dc6203ecd169c79b41868008adc91b9f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans rsync, un outil de copie de fichiers à distance, des attaquants
distants étaient en mesure de contourner le mécanisme de protection
par vérification des arguments en passant l'argument supplémentaire
--protect-args.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
3.1.2-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rsync.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rsync, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rsync">\
https://security-tracker.debian.org/tracker/rsync</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2833.data"
# $Id: $
