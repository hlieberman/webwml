#use wml::debian::translation-check translation="67acc119beab56d48844910baf3d20ea1f059b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a>
pour la bibliothèque d’expressions rationnelles Oniguruma, utilisée notamment
dans les chaînes multioctets de PHP, entrainait un faux positif. En conséquence,
le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a>
a été annulé. Pour référence, voici le texte de l’annonce originelle.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a>

<p>Dans Oniguruma, un attaquant capable de fournir une expression rationnelle
pour la compilation peut outrepasser un tampon d’un octet dans
concat_opt_exact_str dans src/regcomp.c</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 6.1.3-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libonig.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libonig, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libonig">https://security-tracker.debian.org/tracker/libonig</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2431-2.data"
# $Id: $
