#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen. Le
projet « Common Vulnerabilities and Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3672">CVE-2014-3672</a> (XSA-180)

<p>Andrew Sorensen a découvert qu'un domaine HVM peut épuiser l'espace
disque des hôtes en le remplissant du fichier journal.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3158">CVE-2016-3158</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3159">CVE-2016-3159</a> (XSA-172)

<p>Jan Beulich de SUSE a découvert que Xen ne gérait pas correctement les
écritures du bit FSW.ES du matériel lors de l'exécution sur des
processeurs AMD64. Un domaine malveillant peut tirer avantage de ce défaut
pour obtenir des informations sur l'utilisation de l'espace d'adresse et la
synchronisation d'un autre domaine, à un coût relativement faible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3710">CVE-2016-3710</a> (XSA-179)

<p>Wei Xiao et Qinghao Tang de 360.cn Inc ont découvert un défaut de
lecture et d'écriture hors limites dans le module VGA de QEMU. Un
utilisateur client privilégié pourrait utiliser ce défaut pour exécuter du
code arbitraire sur l'hôte avec les privilèges du processus hôte de QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3712">CVE-2016-3712</a> (XSA-179)

<p>Zuozhi Fzz de Alibaba Inc a découvert de possibles problèmes de
dépassement d'entier ou d'accès en lecture hors limites dans le module VGA
de QEMU. Un utilisateur client privilégié pourrait utiliser ce défaut pour
monter un déni de service (plantage du processus de QEMU).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3960">CVE-2016-3960</a> (XSA-173)

<p>Ling Liu et Yihan Lian de l'équipe Cloud Security de Qihoo 360 ont
découvert un dépassement d'entier dans le code de la table des pages
miroir x86. Un client HVM utilisant des tables des pages miroir peut faire
planter l'hôte. Un client PV utilisant des tables des pages miroir
(c'est-à-dire ayant été migrées) avec des superpages PV activées (ce qui
n'est pas le cas par défaut) peut faire planter l'hôte, ou corrompre la
mémoire de l'hyperviseur, menant éventuellement à une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4480">CVE-2016-4480</a> (XSA-176)

<p>Jan Beulich a découvert qu'un traitement incorrect de table des pages
pourrait avoir pour conséquence une augmentation de droits dans une
instance de client Xen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6258">CVE-2016-6258</a> (XSA-182)

<p>Jeremie Boutoille a découvert qu'un traitement incorrect de table des
pages dans des instances paravirtuelles (PV) pourrait avoir pour conséquence
une augmentation de droits du client vers l'hôte.</p></li>

<li>En complément, cette annonce de sécurité de Xen sans référence CVE a
été corrigée : XSA-166

<p>Konrad Rzeszutek Wilk et Jan Beulich ont découvert que le traitement
d'ioreq est éventuellement vulnérable à un problème de lectures multiples.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.1.6.lts1-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-571.data"
# $Id: $
