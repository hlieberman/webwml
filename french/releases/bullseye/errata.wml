#use wml::debian::template title="Debian 11 &mdash; Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="4cfd17ece177c72255c5e9e208c6839889a8d0b9" maintainer="Baptiste Jammet"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problèmes connus</toc-add-entry>
<toc-add-entry name="security">Problèmes de sécurité</toc-add-entry>

<p>
L'équipe de sécurité Debian produit des mises à jour de paquets de la
version stable dans lesquels elle a identifié des problèmes de
sécurité. Merci de consulter les
<a href="$(HOME)/security/">pages concernant la sécurité</a> pour plus
d'informations concernant les problèmes de ce type identifiés dans 
<q>Bullseye</q>.
</p>

<p>
Si vous utilisez APT, ajoutez la ligne suivante à votre fichier
<tt>/etc/apt/sources.list</tt> pour récupérer les dernières mises à jour de
sécurité&nbsp;:
</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>
Ensuite, lancez <kbd>apt update</kbd> suivi de <kbd>apt upgrade</kbd>.
</p>


<toc-add-entry name="pointrelease">Les versions intermédiaires</toc-add-entry>

<p>
Dans le cas où il y a plusieurs problèmes critiques ou plusieurs mises
à jour de sécurité, la version de la distribution est parfois mise à jour.
Généralement, ces mises à jour sont indiquées comme étant des versions
intermédiaires.</p>

<ul>
  <li>La première version intermédiaire, 11.1, a été publiée le
      <a href="$(HOME)/News/2021/20211009">9 octobre 2021</a>.</li>
  <li>La deuxième version intermédiaire, 11.2, a été publiée le
      <a href="$(HOME)/News/2021/20211218">18 décembre 2021</a>.</li>
</ul>

<ifeq <current_release_bullseye> 11.0 "

<p>
À l'heure actuelle, il n'y a pas de mise à jour pour Debian 11.
</p>" "

<p>
Veuillez consulter le <a
href=http://http.us.debian.org/debian/dists/bullseye/ChangeLog>journal des
modifications</a> pour obtenir le détail des modifications entre la
version&nbsp;11.0 et la version&nbsp;<current_release_bulleye/>.
</p>"/>


<p>
Les corrections apportées à la version stable de la distribution passent
souvent par une période de test étendue avant d'être acceptées dans l'archive.
Cependant, ces corrections sont disponibles dans le répertoire <a
href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> de tout miroir de l'archive Debian.
</p>

<p>
Si vous utilisez APT pour mettre à jour vos paquets, vous pouvez installer les
mises à jour proposées en ajoutant la ligne suivante dans votre fichier
<tt>/etc/apt/sources.list</tt>&nbsp;:
</p>

<pre>
  \# Ajouts proposés pour une version 11 intermédiaire
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Ensuite, exécutez <kbd>apt update</kbd> suivi de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Système d'installation</toc-add-entry>

<p>
Pour plus d'informations à propos des errata et des mises à jour du système d'installation, 
consultez la page de <a href="debian-installer/">l'installateur</a>.
</p>
