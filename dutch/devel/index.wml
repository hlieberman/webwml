#use wml::debian::template title="De Debian ontwikkelaarshoek" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="ac395767b81c6001b3d9b14f921c0fa7b6386605"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Hoewel alle informatie op deze pagina en alle links naar andere pagina's publiekelijk beschikbaar zijn, is deze site in de eerste plaats bedoeld voor Debian-ontwikkelaars.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Basisgegevens</a></li>
<li><a href="#packaging">Verpakkingswerkzaamheden</a></li>
<li><a href="#workinprogress">Werk in uitvoering</a></li>
<li><a href="#projects">Projecten</a></li>
<li><a href="#miscellaneous">Varia</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Algemene informatie</a></h2>
      <p>Een lijst van de huidige ontwikkelaars en beheerders, hoe men zich bij het project kan aansluiten, en links naar de database van ontwikkelaars, de statuten, het stemproces, releases, en architecturen.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Debian, de organizatie</a></dt>
        <dd>Meer dan duizend vrijwilligers maken deel uit van het Debian-project. Deze pagina legt de organisatiestructuur van Debian uit, geeft een overzicht van de teams en hun leden, en contactadressen.</dd>
        <dt><a href="$(HOME)/intro/people">De mensen achter Debian</a></dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">Ontwikkelaars van Debian (Debian Developers - DD)</a> (volwaardige leden van het Debian-project) en <a href="https://wiki.debian.org/DebianMaintainer">onderhouders van Debian (Debian Maintainers - DM)</a> dragen bij aan het project. Bekijk de <a href="https://nm.debian.org/public/people/dd_all">lijst met ontwikkelaars van Debian</a> en de <a href="https://nm.debian.org/public/people/dm_all">lijst met onderhouders van Debian</a> om meer te weten over de betrokken personen en over de pakketten die ze beheren. We hebben ook een <a href="developers.loc">wereldkaart met ontwikkelaars van Debian</a> en een <a href="https://gallery.debconf.org/">galerij</a> met afbeeldingen van verschillende Debian-evenementen.</dd>
        <dt><a href="join/">Hoe u kunt aansluiten bij Debian</a></dt>
        <dd>Wilt u bijdragen en u aansluiten bij het project? We zijn altijd op zoek naar nieuwe ontwikkelaars of fans van vrije software met technische vaardigheden. Ga voor meer informatie naar deze pagina.</dd>
        <dt><a href="https://db.debian.org/">Ontwikkelaarsdatabase</a></dt>
        <dd>Bepaalde informatie in deze database is voor iedereen toegankelijk, andere informatie alleen voor ontwikkelaars die zich hebben aangemeld. De database bevat informatie zoals <a href="https://db.debian.org/machines.cgi">projectmachines</a> en <a href="extract_key">GnuPG-sleutels van de ontwikkelaars</a>. Ontwikkelaars met een account kunnen <a href="https://db.debian.org/password.html">hun wachtwoord wijzigen</a> en leren hoe ze het <a href="https://db.debian.org/forward.html">doorsturen van e-mail</a> kunnen instellen voor hun Debian-account. Als u van plan bent een van de Debian-machines te gebruiken, lees dan het <a href="dmup">Beleid in verband met het gebruik van Debian-machines</a>.</dd>
        <dt><a href="constitution">De statuten</a></dt>
        <dd>Dit document beschrijft de organisatiestructuur voor de formele besluitvorming in het project.
        </dd>
        <dt><a href="$(HOME)/vote/">Informatie over het stemmen</a></dt>
        <dd>Hoe we onze leiders kiezen, onze logo's kiezen en in het algemeen hoe het stemmen gebeurt.</dd>
        <dt><a href="$(HOME)/releases/">Releases</a></dt>
        <dd>Deze pagina vermeldt de huidige releases (<a href="$(HOME)/releases/stable/">stable</a>, <a href="$(HOME)/releases/testing/">testing</a> en <a href="$(HOME)/releases/unstable/">unstable</a>) en bevat een index van oude releases en hun codenamen.</dd>
        <dt><a href="$(HOME)/ports/">Verschillende architecturen</a></dt>
        <dd>Debian werkt op veel verschillende architecturen. Deze pagina verzamelt informatie over verschillende Debian-versies, sommige gebaseerd op de Linux-kernel, andere gebaseerd op de kernels van FreeBSD, NetBSD en Hurd.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Het verpakkingswerk</a></h2>
      <p>Links naar ons beleidshandboek en andere documenten in verband met het beleid van Debian, procedures en andere hulpmiddelen voor ontwikkelaars van Debian, en de handleiding voor nieuwe pakketbeheerders.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debian beleidshandboek</a></dt>
        <dd>Deze handleiding beschrijft de beleidsvereisen voor de Debian-distributie. Dit omvat de structuur en de inhoud van het Debian-archief, verschillende aspecten in verband met het ontwerp van het besturingssysteem en de technische vereisten waaraan elk pakket moet voldoen om te worden opgenomen in de distributie.

        <p>Kortom, u <strong>moet</strong> dit lezen.</p>
        </dd>
      </dl>

      <p>Er zijn verschillende andere documenten in verband met het beleid waarin u wellicht geïnteresseerd bent:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />De FHS definieert de mappenstructuur en
        de inhoud van de mappen (de plaats van bestanden);
        compatibiliteit met 3.0 is een verplichting (zie <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">hoofdstuk
        9</a> van het beleidshandboek van Debian).</li>
        <li>Lijst van <a href="$(DOC)/packaging-manuals/build-essential">build-essential pakketten</a>
        <br />Er wordt van u verwacht dat u deze pakketten heeft als u software
        wilt compileren of een pakket of een aantal pakketten wilt bouwen. U
        moet deze niet opnemen in de regel <code>Build-Depends</code> bij het <a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">declareren van relaties</a> tussen pakketten.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Menusysteem</a>
        <br />Debian's menu-itemstructuur; raadpleeg ook de documentatie over
        het <a href="$(DOC)/packaging-manuals/menu.html/">menusysteem</a>
        documentation as well.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacsbeleid</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Javabeleid</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perlbeleid</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Pythonbeleid</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf-specificatie</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Beleid voor databasetoepassingingen</a> (ontwerp)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk-beleid</a> (ontwerp)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Beleid van Debian voor Ada</a></li>
      </ul>

      <p>Raadpleeg ook de <a
      href="https://bugs.debian.org/debian-policy">voorgestelde aanpassingen
      aan het beleid van Debian</a>.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Referentiehandleiding voor ontwikkelaars</a></dt>

        <dd>
        Overzicht van de aanbevolen procedures en de beschikbare hulpmiddelen voor Debian-ontwikkelaars -- nog een <strong>aanrader</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Handleiding voor nieuwe pakkerbeheerders</a></dt>

        <dd>
        Hoe een Debian-pakket gebouwd wordt (in gewone taal), met veel
        voorbeelden. Indien u van plan bent om een ontwikkelaar of
        pakketbeheerder te worden voor Debian, is dit een goed startpunt.
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">Werk in uitvoering: Links voor actieve ontwikkelaars en pakketbeheerders van Debian</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian &lsquo;Testing&rsquo;</a></dt>
  <dd>
    Automatisch gegenereerd vanuit de distributie &lsquo;unstable&rsquo;:
    dit is waar u uw pakketten moet krijgen, wilt u ze in aanmerking laten
    komen voor de volgende release van Debian.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Releasekritieke bugs</a></dt>
  <dd>
    Een lijst met bugs die ervoor kunnen zorgen dat een pakket verwijderd wordt
    uit de distributie &lsquo;testing&rsquo;, of die een vertraging kunnen
    veroorzaken voor de volgende release. Rapporten over bugs met een graad van
    ernst van &lsquo;serious&rsquo; (ernstig) of hoger komen in aanmerking voor
    de lijst, dus zorg ervoor dat u deze bugs tegen uw pakketten zo snel
    mogelijk oplost.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Het bugvolgsysteem (Debian Bug Tracking System - BTS)</a></dt>
    <dd>
    Voor het rapporteren, bespreken en oplossen van bugs. Het BTS is zowel voor
    gebruikers als voor ontwikkelaars nuttig.
    </dd>

  <dt>Informatie over Debian-pakketten</dt>
    <dd>
      De webpagina's voor <a href="https://qa.debian.org/developer.php">pakketinformatie</a>
      en <a href="https://tracker.debian.org/">pakketopvolging</a>
      bezorgen pakketbeheerders een massa waardevolle informatie.
      Ontwikkelaars die andere pakketten wikken opvolgen, kunnen intekenen
      (via e-mail) op een dienst die kopieën verstuurt van e-mails van het
      BTS en meldingen van uploads en installaties. Raadpleeg de <a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">handleiding bij het pakketvolgsysteem</a> voor meer informatie.
    </dd>

    <dt><a href="wnpp/">Pakketten die hulp nodig hebben</a></dt>
      <dd>
      Toekomstige pakketten en pakketten waaraan gewerkt zou moeten
      worden (Work-Needing and Prospective Packages - WNPP), is een lijst
      van Debian-pakketten die een nieuwe beheerder nodig hebben en ook
      van pakketten die nog aan Debian toegevoegd moeten worden.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Het systeem Incoming</a></dt>
      <dd>
      Interne servers voor het archief: dit is waarnaar nieuwe pakketten
      geüpload worden. Aanvaarde pakketten zijn bijna onmiddellijk beschikbaar
      via een webbrowser en worden viermaal per dag verspreid naar
      de <a href="$(HOME)/mirror/">spiegelservers</a>.
      <br />
      <strong>Opmerking:</strong> door de aard van &lsquo;incoming&rsquo;
      raden we het spiegelen ervan niet aan.
      </dd>

    <dt><a href="https://lintian.debian.org/">Rapporten van Lintian</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      is een programma dat nagaat of een pakket conform is aan
      de beleidsrichtlijnen. Ontwikkelaars zouden het vóór iedere upload moeten
      gebruiken.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian &lsquo;Experimental&rsquo;</a></dt>
      <dd>
      De distributie &lsquo;experimental&rsquo; wordt gebruikt als een
      tijdelijke opslagplaats voor zeer experimentele software. Installeer de
      <a href="https://packages.debian.org/experimental/">pakketten uit
      experimental</a> enkel indien u reeds weet hoe u
      &lsquo;unstable&rsquo; moet gebruiken.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">De Wiki van Debian</a></dt>
      <dd>
      De Debian Wiki bevat raadgevingen voor ontwikkelaars en andere
      personen die een bijdrage leveren.
      </dd>
</dl>

<h2><a id="projects">Projecten: interne groepen en projecten</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">De Debian webpagina's</a></li>
<li><a href="https://ftp-master.debian.org/">Het Debian archief</a></li>
<li><a href="$(DOC)/ddp">Het Debian documentatieproject (DDP)</a></li>
<li><a href="https://qa.debian.org/">Het Debian kwaliteitsverzekeringsteam (Quality Assurance - QA)</a></li>
<li><a href="$(HOME)/CD/">cd/dvd-images</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Het ondertekenen van sleutels</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">De coördinatiepagina voor de ondertekening van sleutels</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Het Debian IPv6-project</a></li>
<li><a href="buildd/">Het Auto-buildernetwerk</a> en zijn <a href="https://buildd.debian.org/">bouwlogboeken</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Het Debian Description Translation Project - DDTP</a> (project voor de vertaling van pakketbeschrijvingen)</li>
<li><a href="debian-installer/">Het installatiesysteem van Debian</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian-vrouwen</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a> (Specifieke uitgaven van Debian</li>

</ul>


<h2><a id="miscellaneous">Diverse links</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Opnames</a> van onze conferentietoespraken</li>
<li><a href="passwordlessssh">SSH zo instellen</a> dat het u niet om een wachtwoord vraagt</li>
<li>Hoe u <a href="$(HOME)/MailingLists/HOWTO_start_list">een nieuwe mailinglijst kunt aanvragen</a></li>
<li>Informatie over <a href="$(HOME)/mirror/">een spiegelserver voor Debian opzetten</a></li>
<li>De <a href="https://qa.debian.org/data/bts/graphs/all.png">grafiek van alle bugs</a></li>
<li><a href="https://ftp-master.debian.org/new.html">Nieuwe pakketten</a> die wachten op opname in Debian (NEW-wachtrij)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Nieuwe Debian-pakketten</a> van de voorbije zeven dagen</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Pakketten die verwijderd werden uit Debian</a></li>
</ul>

