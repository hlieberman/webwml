# translation of templates.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-11-03 15:28+0530\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/search.xml.in:7
#, fuzzy
msgid "Debian website"
msgstr "டெபியன் திட்டம்"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr ""

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
#, fuzzy
msgid "Debian"
msgstr "டெபியனுக்கு உதவுக"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr ""

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "ஆம்"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "இல்லை"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "டெபியன் திட்டம்"

#: ../../english/template/debian/common_translation.wml:13
#, fuzzy
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"குனு/ லினக்ஸின் கட்டற்ற வழங்கல்களில் டெபியன் குனு/ லினக்ஸும் ஒன்று. தங்கள் நேரத்தையும் "
"உழைப்பினையும் நல்கும் எண்ணற்ற தன்னார்வலர்களால் அது பராமரிக்கப் பட்டு புதுப்பிக்கப்படுகிறது."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "டெபியன், குனு, லினக்ஸ், யுனிக்ஸ், திறந்த மூலம், கட்டற்ற, டி எப் எஸ் ஜி"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "<a href=\"m4_HOME/\">டெபியன் திட்ட முதற்</a> பக்கத்திற்குத் திரும்புக."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "அகம்"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "உடன்உலாவுவதை தவிர்"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "அறிமுகம்"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "டெபியன் குறித்து"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "தொடர்பு கொள்க"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "வெளியீட்டுத் தகவல்"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "தானங்கள்"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "நிகழ்வுகள்"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "செயதிகள்"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "வழங்கல்கள்"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "ஆதரவு"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "உருவாக்குவோர் கூடல்"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "ஆவணமாக்கம்"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "பாதுகாப்பு விவரம்"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "தேடுக"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "எதுவுமில்லை"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "செல்க"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "அகிலமனைத்தும்"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "தள உதவி"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "ஏனைய"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "டெபியன் பெற"

#: ../../english/template/debian/common_translation.wml:91
#, fuzzy
msgid "The Debian Blog"
msgstr "டெபியன் புத்தகங்கள்"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
msgid "Debian Micronews"
msgstr "டெபியன் திட்டம்"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "டெபியன் திட்டம்"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "கடைசியாக மாற்றப் பட்டது"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"இணையகப் பக்கங்கள் குறித்த கருத்துக்கள், விமர்சனங்கள் மற்றும் பரிந்துரைகளை எங்கள் மடலாடற் <a "
"href=\"mailto:debian-doc@lists.debian.org\">குழுவிற்கு அனுப்பவும்</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "தேவையில்லை"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "கிடைக்கப்பெறவில்லை"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "பொருத்தமில்லா"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "1.1 வெளியீட்டில்"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "1.3 வெளியீட்டில்"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "2.0 வெளியீட்டில்"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "2.1 வெளியீட்டில்"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "2.2 வெளியீட்டில்"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
#, fuzzy
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"இணையத் தளத்திலுள்ள பிரச்சனைக் குறித்து தெரிவிக்க, <a href=\"mailto:debian-www@lists."
"debian.org\">debian-www@lists.debian.org</a> க்கு மடலிடவும்.  தொடர்பு கொள்ள "
"விழைந்தால் டெபியன்<a href=\"m4_HOME/contact\">தொடர்பு பக்கத்தை பார்க்கவும்</a>."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "கடைசியாக மாற்றப் பட்டது"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "பதிப்புரிமை"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr ""

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"<a href=\"m4_HOME/license\" rel=\"copyright\">உரும விவரங்களைப் பார்வையிடவும்</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"பொதுநோக்கத்திற்கான மென்பொருளின் பதிவு பெற்ற <a href=\"m4_HOME/trademark\">வணிக "
"முத்திரை</a> டெபியனாகும்."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "இப்பக்கம் கீழ்கண்ட மொழிகளிலும் கிடைக்கப் பெறுகிறது:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "ஆவணத்தின் இயலபிருப்பு மொழியினை அமைப்பது <a href=m4_HOME/intro/cn>எப்படி</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "சர்வதேச டெபியன்"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "பங்குதாரர்கள்"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "வாராந்திர டெபியன் செய்திகள்"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "வாராந்திர செய்திகள்"

#: ../../english/template/debian/links.tags.wml:16
#, fuzzy
msgid "Debian Project News"
msgstr "டெபியன் திட்டம்"

#: ../../english/template/debian/links.tags.wml:19
#, fuzzy
msgid "Project News"
msgstr "வாராந்திர செய்திகள்"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "வெளியீட்டுத் தகவல்"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "டெபியன் பொதிகள்"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "பதிவிறக்கம்"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "வட்டில்&nbsp;டெபியன்"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "டெபியன் புத்தகங்கள்"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
msgid "Debian Wiki"
msgstr "டெபியனுக்கு உதவுக"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "மடலாடற் குழு பெட்டகம்"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "மடலாடற் குழுக்கள்"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "சமூக ஒப்பந்தம்"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr ""

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr ""

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "டெபியன் இணையத் தளப்ப பக்கங்களின் வழிகாட்டி"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "உருவோக்குவோரின் தரவுக் களன்"

#: ../../english/template/debian/links.tags.wml:64
#, fuzzy
msgid "Debian FAQ"
msgstr "டெபியன் புத்தகங்கள்"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "டெபியன் கொள்கைக் கையேடு"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "உருவாக்குவோரது பார்வைக்கு"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "புதியப் பராமரிப்பாளரது கையேடு"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "முக்கியமான வழுக்களை வெளியிடுக"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "லின்டியன் அறிக்கைகள்"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "பயனர்களது மடலாடற் குழுக்களுக்கான பெட்டகங்கள்"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "உருவாக்குவோரதுளது மடலாடற் குழுக்களுக்கான பெட்டகங்கள்"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "சர்வதேச/ தன் மொழியாக்க மடலாடற் குழுக்களுக்கான பெட்டகங்கள்"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "பெயர்த்தலுக்கான மடலாடற் குழுக்களுக்கான பெட்டகம்"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "வழு நோட்ட அமைப்பு மடலாடற் குழுவுக்கான பெட்டகம்"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "ஏனைய மடலாடற் குழுக்களுக்கான பெட்டகங்கள்"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "கட்டற்ற மென்பொருள்"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "உருவாக்கம்"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "டெபியனுக்கு உதவுக"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "வழுத் தாக்கல்"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "துறைகள்/கட்டமைப்புகள்"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "நிறுவல் கையேடு"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "வட்டு வழங்குவோர்"

#: ../../english/template/debian/links.tags.wml:125
#, fuzzy
msgid "CD/USB ISO images"
msgstr "ஐஎஸ்ஓ மாதிரிகளுக்கான வட்டு"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "பிணைய நிறுவல்"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "முன்-நிறுவப்பட்டபட்ட"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "டெபியன் கல்வித் திட்டம்"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "அலியோத் &ndash; டெபியன் GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "தர உத்தரவாதம்"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "பொதி நோட்ட அமைப்பு"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "பொதிகள் குறித்த டெபியன் உருவாக்குநரது கண்ணோட்டம்"

#: ../../english/template/debian/navbar.wml:10
#, fuzzy
msgid "Debian Home"
msgstr "டெபியன் திட்டம்"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "இவ்வாண்டிற்கு எந்த உருப்படியும் இல்லை."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "பரிந்துரைக்கப்பட்ட"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "விவாதத்தில்"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "வாக்களிக்க வாய்ப்புள்ளது"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "நிறைவுற்றது"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "திரும்பப் பெறப்பட்டது"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "எதிரவரும் நிகழ்வுகள்"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "கடந்த கால நிகழ்வுகள்"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(புதிய மீள்பார்வை)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "அறியச்செய்க"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr ""

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>குறிப்பு:</em> The <a href=\"$link\">முதல் ஆவணம்</a> இம்மொழிபெயர்ப்பைக் "
"காட்டிலும் புதிதாப உள்ளது."

#: ../../english/template/debian/translation-check.wml:43
#, fuzzy
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"எச்சரிக்கை! இம்மொழிபெயர்ப்பு பழையதாகத் தோன்றுகிறதுபுhe <a href=\"$linkமுதலானதைப்nal< "
"பார்க்கவும்</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>குறிப்பு:</em> இம்மொழி பெயர்ப்பின் முதல் ஆவணம் இனியும் இல்லை."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "<a href=\"../\">டெபியனைப் பயன்படுத்துவோர் யார்?</a> பக்கத்திற்கு திரும்புக."

#~ msgid "More information"
#~ msgstr "மேறகொண்டு விவரமறிய"

#~ msgid "Upcoming Attractions"
#~ msgstr "எதிர்வருபவை"

#~ msgid "link may no longer be valid"
#~ msgstr "இணைப்பு இனி செல்லுபடியாகாதிருக்கலாம்"

#~ msgid "When"
#~ msgstr "எப்பொழுது"

#~ msgid "Where"
#~ msgstr "எங்கே"

#~ msgid "More Info"
#~ msgstr "மேலும் விவரங்களுக்கு"

#~ msgid "Debian Involvement"
#~ msgstr "டெபியன் ஈடுபாடு"

#~ msgid "Main Coordinator"
#~ msgstr "முதன்மை ஒருங்கிணைப்பாளர்"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>திட்ட</th><th>ஒருங்கிணைப்பாளர்</th>"

#~ msgid "Related Links"
#~ msgstr "தொடர்புடைய இணைப்புகள்"

#~ msgid "Latest News"
#~ msgstr "புதிய செய்தி"

#~ msgid "Download calendar entry"
#~ msgstr "நாட்காட்டிப் பதிவை பதிவிறக்குக"

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "ஏனைய <a href=\"./\">டெபியன் செயதிகளுக்குத் </a> திரும்பவும் || <a href="
#~ "\"m4_HOME/\">டெபியன் திட்ட முதற் பக்கம்</a>."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (செயற் படாத இணைப்பு)"

#, fuzzy
#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "இச்செய்தியறிக்கையினை வாரந்தோறும் மடலாகப் பெற, <a href=\"https://lists.debian."
#~ "org/debian-news/\">டெபியன்செய்தி மடலாடற் குழுவில் இணையவும்</a>."

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr "செய்தியறிக்கையின்<a href=\"../../\">முந்தைய பதிப்புகள்</a> கிடைக்கின்றன."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />டெபியன் வாராந்திரச் செய்தியின் ஆசிரியர்<a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />டெபியன் வாராந்திரச் செய்தியின் ஆசிரியர்கள் <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />This issue of Debian Weekly News was edited by "
#~| "<a href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />டெபியனின் இவ்வாரச் செய்தியின் ஆசிரியர் <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~| "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />டெபியனின் இவ்வாரச் செய்தியின் ஆசிரியர்கள்<a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />%s இதனை மொழிபெயர்த்தார்."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />%s இதனை மொழிபெயர்த்தவர்கள் ஆவர்."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />%s இதனை மொழிபெயர்த்தார்."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />%s இதனை மொழிபெயர்த்தவர்கள் ஆவர்."

#~ msgid "List of Speakers"
#~ msgstr "பேச்சாளர் பட்டியல்"

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "<a href=\"./\">டெபியன்</a> பேச்சாளர் பக்கத்திற்குத் திரும்புக."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "இச்செய்தியறிக்கையினை வாரந்தோறும் மடலாகப் பெற, <a href=\"https://lists.debian."
#~ "org/debian-news/\">டெபியன்செய்தி மடலாடற் குழுவில் இணையவும்</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />டெபியன் வாராந்திரச் செய்தியின் ஆசிரியர்<a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />டெபியன் வாராந்திரச் செய்தியின் ஆசிரியர்கள் <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />டெபியனின் இவ்வாரச் செய்தியின் ஆசிரியர் <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />டெபியனின் இவ்வாரச் செய்தியின் ஆசிரியர்கள்<a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid "No requests for adoption"
#~ msgstr "தத்தெடுப்பதற்கான கோரிக்கைகள் எதுவும் இல்லை"

#~ msgid "No orphaned packages"
#~ msgstr "கைவிடப்பட்ட பொதிகள் எதுவும் இல்லை"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "தத்தெடுக்கும் பொருட்டு எந்தவொரு பொதியும் இல்லை"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "பொதியாக்கும்டுக்கும் பொருட்டு எந்தவொரு பொதியும் இல்லை"

#~ msgid "No Requested packages"
#~ msgstr "எந்த பொதியும் கோரப்படவில்லை"

#~ msgid "No help requested"
#~ msgstr "எந்த உதவியும் கோரப்படவில்லை"

#~ msgid "in adoption since today."
#~ msgstr "இன்றிற்றிலிருந்து தத்தெடுக்கப் பட்டுள்ளது."

#~ msgid "in adoption since yesterday."
#~ msgstr "நேற்றிலிருந்து தத்தெடுக்கப் பட்டுள்ளது."

#~ msgid "%s days in adoption."
#~ msgstr "%s நாட்களாக தத்தெடுக்கப் பட்டுள்ளது."

#~ msgid "in preparation since today."
#~ msgstr "இன்றிலிருந்து தயாரிக்கப் பட்டு வருகின்றது."

#~ msgid "in preparation since yesterday."
#~ msgstr "நேற்றிலிருந்து தயாரிக்கப் பட்டு வருகிறது."

#~ msgid "%s days in preparation."
#~ msgstr "தயாரிப்பில் %s நாட்கள்."

#~ msgid "requested today."
#~ msgstr "இன்று கோரப்பட்டது."

#~ msgid "requested yesterday."
#~ msgstr "நேற்று கோரப்பட்டது"

#~ msgid "requested %s days ago."
#~ msgstr "%s நாட்களுக்கு முன் கோரப்பட்டது."

#~ msgid "package info"
#~ msgstr "பொதி விவரம்"

#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "<void id=\"dc_pik\" />பிம்பக் கருவியின் மாதிரி"

#~ msgid "License Information"
#~ msgstr "உரிம விவரம்"

#~ msgid "DLS Index"
#~ msgstr "டிஎல்எஸ் வரிசை"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG கேள்விகள்"

#~ msgid "Debian-Legal Archive"
#~ msgstr "டெபியன்-சட்ட பெட்டகம்"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, வெளியீடு %s: %s"

#~ msgid "Date published"
#~ msgstr "பதிப்பிக்கப் பட்ட தேதி"

#~ msgid "License"
#~ msgstr "உரிமம்"

#~ msgid "Version"
#~ msgstr "வெளியீடு"

#~ msgid "Summary"
#~ msgstr "தொகுப்புரை"

#~ msgid "Justification"
#~ msgstr "நியாயம்"

#~ msgid "Discussion"
#~ msgstr "விவாதம்"

#~ msgid "Original Summary"
#~ msgstr "முதல் தொகுப்புரைை"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "முதல் உரைத் தொகுப்பினை <summary-author/> <a href=\"<summary-url/>"
#~ "\">பெட்டகத்தில்</a> காணலாம்."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "உரைத் தொகுப்பினைத் தயாரித்தவர் <summary-author/>."

#~ msgid "License text (translated)"
#~ msgstr "உரிம உரை (மொழிபெயர்க்கப் பட்டது)"

#~ msgid "License text"
#~ msgstr "உரிம உரை"

#~ msgid "free"
#~ msgstr "கட்டற்ற"

#~ msgid "non-free"
#~ msgstr "கட்டுடைய"

#~ msgid "not redistributable"
#~ msgstr "மீண்டும் விநியோகிக்கத் தகாத"

#~ msgid "Free"
#~ msgstr "கட்டற்ற"

#~ msgid "Non-Free"
#~ msgstr "கட்டுடைய"

#~ msgid "Not Redistributable"
#~ msgstr "மீண்டும் விநியோகிக்கத் தகாத"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "டெபியன் உரிமங்கள் குறித்து அறிய <a href=\"./\">உரிமத் தகவல்</a> பக்கத்தின் "
#~ "துணையினை நாடுக."

#~ msgid "Vote"
#~ msgstr "வாக்களி"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "விளைவொன்றை&nbsp;வாசிக்கவும்&nbsp;"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "பரிந்துரையொன்றை&nbsp;பின்பற்றவும்&nbsp;"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "பரிந்துரையொன்றை&nbsp;அமல் படுத்துக&nbsp;"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "பரிந்துரையொன்றை&nbsp;சமர்பிக்கவும்&nbsp;"

#~ msgid "How&nbsp;To"
#~ msgstr "எப்படி"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "&nbsp;வாக்களிப்பின்&nbsp;முதற் பக்கத்திற்கு"

#~ msgid "Other"
#~ msgstr "ஏனைய"

#~ msgid "Withdrawn"
#~ msgstr "திரும்பப் பெறப் பட்டது"

#~ msgid "Decided"
#~ msgstr "தீர்மானிக்கப்பட்டது"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "வாக்கெடுப்பு&nbsp;நடைபெறுகிறது "

#~ msgid "In&nbsp;Discussion"
#~ msgstr "விவாதத்தில்"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "ஆதரவாளர்களுக்காக&nbsp;காத்திருக்கிறொம்&nbsp;"

#~ msgid "Outcome"
#~ msgstr "விளைவு"

#~ msgid "Forum"
#~ msgstr "தருக்கம்"

#~ msgid "Ballot"
#~ msgstr "வாக்கெடுப்பு"

#~ msgid "Minimum Discussion"
#~ msgstr "குறைந்த பட்ச கலந்தாயவு"

#~ msgid "Quorum"
#~ msgstr "குறைந்தபட்ச தேவை"

#~ msgid "Data and Statistics"
#~ msgstr "தரவும் புள்ளி விவரமும்"

#~ msgid "Majority Requirement"
#~ msgstr "பெரும்பான்மைத் தேவை"

#~ msgid "Proceedings"
#~ msgstr "நிகழ்பவை"

#~ msgid "Amendments"
#~ msgstr "திருத்தங்கள்"

#~ msgid "Amendment Text B"
#~ msgstr "திருத்த நொடிகள் இ"

#~ msgid "Amendment Seconds B"
#~ msgstr "திருத்த நொடிகள் இ"

#~ msgid "Amendment Proposer B"
#~ msgstr "திருத்தத்தைப் பரிந்துரைத்தவர் இ"

#~ msgid "Amendment Text A"
#~ msgstr "திருத்த உரை அ"

#~ msgid "Amendment Seconds A"
#~ msgstr "திருத்த நொடிகள் அ"

#~ msgid "Amendment Proposer A"
#~ msgstr "திருத்தத்தைப் பரிந்துரைத்தவர் அ"

#~ msgid "Amendment Text"
#~ msgstr "திருத்த உரைகள்"

#~ msgid "Amendment Seconds"
#~ msgstr "திருத்த நொடிகள்"

#~ msgid "Amendment Proposer"
#~ msgstr "திருத்தத்தைப் பரிந்துரைத்தவர்"

#~ msgid "Choices"
#~ msgstr "தேர்வுகள்"

#~ msgid "Proposal F"
#~ msgstr "பரிந்துரை ஒ"

#~ msgid "Proposal E"
#~ msgstr "பரிந்துரை ஐ"

#~ msgid "Proposal D"
#~ msgstr "பரிந்துரை எ"

#~ msgid "Proposal C"
#~ msgstr "பரிந்துரை உ"

#~ msgid "Proposal B"
#~ msgstr "பரிந்துரை இ"

#~ msgid "Proposal A"
#~ msgstr "பரிந்துரை அ"

#~ msgid "Text"
#~ msgstr "உரை"

#~ msgid "Opposition"
#~ msgstr "எதிர்தரப்பு"

#~ msgid "Proposal F Seconds"
#~ msgstr "பரிந்துரை ஒ நொடிகள்"

#~ msgid "Proposal E Seconds"
#~ msgstr "பரிந்துரை ஐ நொடிகள்"

#~ msgid "Proposal D Seconds"
#~ msgstr "பரிந்துரை எ நொடிகள்"

#~ msgid "Proposal C Seconds"
#~ msgstr "பரிந்துரை உ நொடிகள்"

#~ msgid "Proposal B Seconds"
#~ msgstr "பரிந்துரை இ நொடிகள்"

#~ msgid "Proposal A Seconds"
#~ msgstr "பரிந்துரை அ நொடிகள்"

#~ msgid "Seconds"
#~ msgstr "நொடிகள்"

#~ msgid "Proposal F Proposer"
#~ msgstr "பரிந்துரை ஒ வினைப் பரிந்துரைத்தவர்"

#~ msgid "Proposal E Proposer"
#~ msgstr "பரிந்துரை ஐ வினைப் பரிந்துரைத்தவர்"

#~ msgid "Proposal D Proposer"
#~ msgstr "பரிந்துரை எ வினைப் பரிந்துரைத்தவர்"

#~ msgid "Proposal C Proposer"
#~ msgstr "பரிந்துரை உ வினைப் பரிந்துரைத்தவர்"

#~ msgid "Proposal B Proposer"
#~ msgstr "பரிந்துரை இ வினைப் பரிந்துரைத்தவர்"

#~ msgid "Proposal A Proposer"
#~ msgstr "பரிந்துரை அ வினைப் பரிந்துரைத்தவர்"

#~ msgid "Proposer"
#~ msgstr "பரிந்துரைத்தவர்"

#~ msgid "Platforms"
#~ msgstr "கட்டமைப்புகள்"

#~ msgid "Debate"
#~ msgstr "விவாதம்"

#~ msgid "Nominations"
#~ msgstr "பரிந்துரைகள்"

#~ msgid "Time Line"
#~ msgstr "காலவரை"

#~ msgid "Date"
#~ msgstr "தேதி"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "வட்டுக்களுக்கான ஆங்கில மொழியில்<a href=\"/MailingLists/disclaimer\">பொது "
#~ "மடலாடற் குழு</a> :"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />ஏனைய"

#~ msgid "net_install"
#~ msgstr "பிணைய_நிறுவல்"

#~ msgid "buy"
#~ msgstr "வாங்குக"

#~ msgid "http_ftp"
#~ msgstr "மீபநெ_கோபநெ"

#~ msgid "jigdo"
#~ msgstr "ஜிக்டோ"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />கேள்விகள்"

#~ msgid "debian_on_cd"
#~ msgstr "வட்டில்_டெபியன்"

#~ msgid "Debian CD team"
#~ msgstr "டெபியன் வட்டுக் குழு"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />பிம்ப வெளியீட்டுத் தகவல்"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />டாரன்டு பயன்படுத்தி பதிவிறக்க"

#, fuzzy
#~| msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_torrent\" />டாரன்டு பயன்படுத்தி பதிவிறக்க"

#, fuzzy
#~| msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_torrent\" />டாரன்டு பயன்படுத்தி பதிவிறக்க"

#, fuzzy
#~| msgid "<void id=\"misc-bottom\" />misc"
#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"misc-bottom\" />ஏனைய"

#, fuzzy
#~| msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_torrent\" />டாரன்டு பயன்படுத்தி பதிவிறக்க"

#, fuzzy
#~| msgid "Network install"
#~ msgid "Network Install"
#~ msgstr "பிணைய நிறுவல்"

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "<a href=\"./\">டெபியன் ஆலோசகர்கள் பக்கத்திற்குத்</a> திரும்புக."

#~ msgid "List of Consultants"
#~ msgstr "ஆலோசகர்கள் பட்டியல்"

#~ msgid "Rating:"
#~ msgstr "தரமதிப்பீடு:"

#~ msgid "Nobody"
#~ msgstr "யாருமில்லை"

#~ msgid "Taken by:"
#~ msgstr "ஏற்றவர்:"

#~ msgid "More information:"
#~ msgstr "மேலும் விவரங்களுக்கு:"

#~ msgid "Select a server near you"
#~ msgstr "தங்களுக்கு அருகாமையிலுள்ள வழங்கியினைத் தேர்வுச் செய்க"

#~ msgid "Visit the site sponsor"
#~ msgstr "தள ஆதரவாளரை அறிய"
