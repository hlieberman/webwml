msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2010-11-28 23:20+0700\n"
"Last-Translator: Mahyuddin Susanto <udienz@gmail.com>\n"
"Language-Team: Debian Indonesia Translator <debian-l10n@debian-id.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Indonesian\n"
"X-Poedit-Country: INDONESIA\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "sekarang"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "anggota"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "Stable Release Manager"
msgstr "Manager Rilis"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "ketua"

#: ../../english/intro/organization.data:48
#, fuzzy
msgid "assistant"
msgstr "Asisten FTP"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "sekretaris"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Petugas"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribusi"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
#, fuzzy
msgid "Publicity team"
msgstr "Publikasi"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:305
msgid "Support and Infrastructure"
msgstr "Dukungan dan infrastruktur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Ketua"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Komisi Teknis"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Sekretaris"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Proyek Pengembangan"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Arsip FTP"

#: ../../english/intro/organization.data:114
#, fuzzy
msgid "FTP Masters"
msgstr "FTP Master"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Asisten FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Menagement rilis"

#: ../../english/intro/organization.data:138
#, fuzzy
msgid "Release Team"
msgstr "Catatan Rilis"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Jaminan Kualitas"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Tim Instalasi Sistem"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Catatan Rilis"

#: ../../english/intro/organization.data:153
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD Image"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Produksi"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Pengujian"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:168
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Dukungan dan infrastruktur"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:177
#, fuzzy
msgid "Buildd administration"
msgstr "Administrasi sistem"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Dokumentasi"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Daftar paket yang butuh dikerjakan dan paket yang diharapkan"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Kontak press"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Halaman web"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:235
#, fuzzy
msgid "Debian Women Project"
msgstr "Proyek Pengembangan"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Acara"

#: ../../english/intro/organization.data:259
#, fuzzy
msgid "DebConf Committee"
msgstr "Komisi Teknis"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Program Partner"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Koordiasi donasi perangkat keras"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:295
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:298
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:301
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Bug Tracking System"
msgstr "Sistem Bug Tracking"

#: ../../english/intro/organization.data:313
#, fuzzy
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Asministrasi Milis"

#: ../../english/intro/organization.data:321
#, fuzzy
msgid "New Members Front Desk"
msgstr "New Maintainers Front Desk"

#: ../../english/intro/organization.data:327
msgid "Debian Account Managers"
msgstr "Pengelola akun Debian"

#: ../../english/intro/organization.data:331
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:332
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Pengelola Keyring (PGP dan GPG)"

#: ../../english/intro/organization.data:336
msgid "Security Team"
msgstr "Tim keamanan"

#: ../../english/intro/organization.data:347
msgid "Policy"
msgstr "Peraturan"

#: ../../english/intro/organization.data:350
msgid "System Administration"
msgstr "Administrasi sistem"

#: ../../english/intro/organization.data:351
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Ini adalah alamat yang digunakan ketika menemui satu masalah dalam mesin "
"Debian, termasuk masalah kata sandi atau anda membutuhkan instalasi paket"

#: ../../english/intro/organization.data:361
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Jika anda mempunyai masalah di mesin Debian, silakan lihat halaman <a href="
"\"https://db.debian.org/machines.cgi\">mesin Debian</a>, yang mengandung "
"informasi administrasi per mesin"

#: ../../english/intro/organization.data:362
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator Direktori LDAP pengembang"

#: ../../english/intro/organization.data:363
msgid "Mirrors"
msgstr "Cermin"

#: ../../english/intro/organization.data:369
msgid "DNS Maintainer"
msgstr "Pengelola DNS"

#: ../../english/intro/organization.data:370
msgid "Package Tracking System"
msgstr "Sistem tracking paket"

#: ../../english/intro/organization.data:372
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:379
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:383
#, fuzzy
msgid "Salsa administrators"
msgstr "Administrator Alioth"

#~ msgid "Consultants Page"
#~ msgstr "Halaman konsultan"

#~ msgid "CD Vendors Page"
#~ msgstr "Halaman penyedia CD"

#~ msgid "Ports"
#~ msgstr "Ports"

#~ msgid "Special Configurations"
#~ msgstr "Konfigurasi Instimewa"

#~ msgid "Laptops"
#~ msgstr "Laptop"

#~ msgid "Firewalls"
#~ msgstr "Firewall"

#~ msgid "Embedded systems"
#~ msgstr "Sistem embedded"

#~ msgid "User support"
#~ msgstr "Dukungan pengguna"

#~ msgid "Alioth administrators"
#~ msgstr "Administrator Alioth"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Tim keamanan"

#~ msgid "Security Audit Project"
#~ msgstr "Proyek audit keamanan"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribusi Debian Racikan"

#~ msgid "Release Assistants"
#~ msgstr "Asisten Rilis"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Manager rilis untuk ``stable''"

#~ msgid "Vendors"
#~ msgstr "Penyedia"

#~ msgid "APT Team"
#~ msgstr "Tim APT"

#~ msgid "Handhelds"
#~ msgstr "Handhelds"

#~ msgid "Mailing List Archives"
#~ msgstr "Arsip Milis"

#~ msgid "Security Testing Team"
#~ msgstr "Tim penguji keamanan"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinasi penandatanganan kunci"

#~ msgid "Accountant"
#~ msgstr "Akuntan"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Sistem operasi universal sebagai desktop anda"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian untuk organisasi nirlaba"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux untuk Komputasi Enterprise"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribusi Debian Multimedia"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Ini belum menjadi Proyek internal Debian tapi sudah diumumkan akan "
#~ "disertakan "

#~ msgid "Publicity"
#~ msgstr "Publikasi"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Tim Instalasi Sistem"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian untuk edukasi"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "Debian untuk medis dan penelitian"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian untuk penyandang cacat"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian di kantor hukum"

#~ msgid "Debian for education"
#~ msgstr "Debian untuk edukasi"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian untuk medis dan penelitian"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian untuk anak kecil dari 1 sampai 99"

#~ msgid "Individual Packages"
#~ msgstr "Paket individu"
